function [video] = getvideo( videoNumber,willCrop,sampleNum)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
import Utils.* DM_AD.* videoArticle.*
folder = 'data';
Num = {'1','2'};
videoImg = dir([folder '\s' Num{videoNumber} '_*.jpg']);
videoImg = {videoImg.name};

if nargin < 3 || isempty(sampleNum)
    sampleNum = length(videoImg);
end

if(willCrop)
    vidMat = zeros(240*160*3,sampleNum);
else
    vidMat = zeros(240*320*3,sampleNum);
end
for i=1:sampleNum
    a = imread([folder '\' videoImg{i}]);
    if(willCrop)
        a = a(:,161:320,:);
    end
    vidMat(:,i)=a(:);
end
    video = vidMat;
end
