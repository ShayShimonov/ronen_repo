
t=tic;
numOfSamples = 1500;
numOfSamples = 2000;
vidMat = getvideo( 2,1,numOfSamples);
lSamp = size(vidMat,2);
toc(t)
% create kernel matrix
%% 
epsilon = sum(std(vidMat,0,2));
%% 1
numNeighbors = 3;
d = pdist2(vidMat',vidMat');
[B,IX]=sort(d,2);   
eps=(mean(B(:,[2:numNeighbors+1])'.^2));    
kernel =  exp( - diag(1./sqrt(eps)) * (d.^2) * diag(1./sqrt(eps)) ); 

%% 2
s0=sum(kernel); 
K1 = diag(1./(s0) ) * kernel * diag(1./(s0) );
s1=sum(K1);
K2 = diag(1./sqrt(s1) ) * K1 * diag(1./sqrt(s1));

%% 3
t=4;
[U,S,V] = svd(K2);
svals=diag(S);
UWighted = diag(1./U(:,1)) * U;
MapEmbd = UWighted * diag( svals.^t );

%% 4
figure
scatter(MapEmbd(:,2),MapEmbd(:,3),20,'filled');

%%kernel = exp((- pdist2(vidMat',vidMat').^2)/(2*epsilon));
%%
D = diag(sum(kernel,2));
Prob = diag((1./diag(D)))*kernel;

[U,S,V] = svd(Prob);
%%
dim=2;
MarkT=40;
eigV_T = diag(S).^MarkT;
phi = zeros(dim,lSamp);
for i=1:lSamp
    temp = U(i,:);
    phi(:,i)= (temp(1:dim)').*(eigV_T(1:dim));
end
DM_dist=pdist2(phi',phi');
plot(phi(1,:),phi(2,:));
%% the probmlem: D is not sorted by eigen values