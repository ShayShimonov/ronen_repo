import Utils.* DM_AD.* videoArticle.*
t=tic;
vidMat = getvideo( 2,1,1500);
lSamp = size(vidMat,2);
toc(t)
% create kernel matrix
%% 
% epsilon = sum(std(vidMat,0,2));
% kernel = exp((-(pdist2(vidMat',vidMat')).^2)/(2*epsilon));
% D = diag(sum(kernel,2));
numNeighbors = 3;
d = pdist2(vidMat',vidMat');
[B,IX]=sort(d,2);   
eps=(mean(B(:,[2:numNeighbors+1])'.^2));    
kernel =  exp( - diag(1./sqrt(eps)) * (d.^2) * diag(1./sqrt(eps)) ); 
D = diag(sum(kernel,2));
Prob = diag((1./diag(D)))*kernel;
[U,S,V] = svd(Prob);
%%
dim=2;
MarkT=40;
eigV_T = diag(S).^MarkT;
phi = zeros(dim,lSamp);
for i=1:lSamp
    temp = U(i,:);
    phi(:,i)= (temp(1:dim)').*(eigV_T(1:dim));
end
DM_dist=pdist2(phi',phi');
scatter(phi(1,:),phi(2,:));
