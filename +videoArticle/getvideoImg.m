function [ img ] = getvideoImg( videoNumber,willCrop,index)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
import Utils.* DM_AD.* videoArticle.*
folder = 'data';
Num = {'1','2'};
videoImg = dir([folder '\s' Num{videoNumber} '_*.jpg']);
videoImg = {videoImg.name};
a = imread([folder '\' videoImg{index}]);
if(willCrop)
 a=a(:,161:320,:);
end
img =a;
end

