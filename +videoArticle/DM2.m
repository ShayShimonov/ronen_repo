t=tic;
vidMat = getvideo( 2,1,1500);
lSamp = size(vidMat,2);
toc(t)
% create kernel matrix
%% 
% epsilon = sum(std(vidMat,0,2));
% kernel = exp((-pdist2(vidMat',vidMat'))/(2*epsilon));
numNeighbors = 3;
d = pdist2(vidMat',vidMat');
[B,IX]=sort(d,2);   
eps=(mean(B(:,[2:numNeighbors+1])'.^2));    
kernel =  exp( - diag(1./sqrt(eps)) * (d.^2) * diag(1./sqrt(eps)) ); 
%%
D1 = diag((sum(kernel,2)).^-1);
K2= D1*kernel*D1;
D2 = diag((sqrt(sum(K2,2))).^-1);
Kf= D2*kernel*D2;
[U,S,V] = svd(Prob);
eig0 = repmat(U(:,1),1,lSamp-1);
Neig = U(:,2:(lSamp))./(eig0);
%%
dim=2;
MarkT=70;
eigV_T = diag(S).^MarkT;
eigV_T = eigV_T(2:lSamp);
phi = zeros(dim,lSamp);
for i=1:lSamp
    temp = Neig(i,:);
    phi(:,i)= (temp(1:dim)').*(eigV_T(1:dim));
end
DM_dist=pdist2(phi',phi');
figure;
scatter(phi(1,:),phi(2,:));
