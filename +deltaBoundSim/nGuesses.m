%{ compAre between different values of n guesses}%
ReverseOrder = 0;
deltaSimNum =1;
FILEPATH = 'D:\Users\Shay\Documents\ronen_repo\simulationFiles\deltaBound\old\';
if(ReverseOrder)
    load([(strcat(FILEPATH, 'RdeltaSim')),num2str(deltaSimNum)]);
    deltaZSim =RdeltaZSim;
    deltaYSim =RdeltaYSim;
    n1Sim=Rn2Sim;
    n2Sim=Rn1Sim;
    [epss1,epss2] = meshgrid(epss);
else
    load([(strcat(FILEPATH, 'deltaSim')),num2str(deltaSimNum)]);
    [epss2,epss1] = meshgrid(epss);
end
NUMBER_OF_POINTS = size(xy,2);
n1 = NUMBER_OF_POINTS.*epss1.^((dims(1)+dims(2)));
n2 = NUMBER_OF_POINTS.*(epss1+epss2).^((dims(1)+dims(3)));
%% plot n
figure()
c0 =surf(epss1,epss2,n1Sim);
hold on;
c1 = surf(epss1,epss2,n1);
hold off;
legend('real','calc');
c0.FaceColor = 'b';
c1.FaceColor = 'm';
title('n1 calc & real');
% n2Sim = CutAreaToNanSat(n2Sim,SatIndY,areaY);
n2calc = beta./bounds(2);
% n2calc = CutAreaToNanSat(n2calc,SatIndY,areaY);


function [] =plotNGuessVsReal(eps1, eps2, n_guess,n_real, title_of)
    figure()
    d0 =surf(eps1,eps2,n_real);
    hold on;
    d1 = surf(eps1,eps2,n_guess);
    hold off;
    legend('real','calc');
    d0.FaceColor = 'b';
    d1.FaceColor = 'm';
    title(title_of);
end