function [deltaZSim, deltaYSim ,xGT,xy,xz,epss] = createSimValues_knn( deltaSimNum,EpsPoints,bounds,dims )
%{
-Function name: createSimValues
-Description:
* crates mat file- wrapper to sim
-Inputs:
* deltaSimNum - number of Sim to save
* bounds - row vector which specifies disturpution upper bound assuming lower bound is zero
* dims - dimentions of hidden variable distrebution
* EpsPoints - number of points (different values) in the epsilon axe
* ReverseOrder - if true reverse order of AD kernel is taken
-Outputs:
* deltaZSim - values calculated feom simulation
* deltaYSim - values calculated feom simulation
* xGT - values calculated feom simulation
* xy - values calculated feom simulation
* xz - values calculated feom simulation
* epss - values calculated feom simulation
-Reurn values:
* saved mat files of outbut in both regular and reverse order
-Notes:
* this function uses the knn kernels instead of the regular ones
%}
%% imports
import Utils.* DM_AD.* deltaBoundSim.* ADsim.*
%% contants
NUMBER_OF_POINTS_IN_DIST = 1000;
EPSILON_LOWER_BOUND = sqrt(2)/sqrt(NUMBER_OF_POINTS_IN_DIST);
EPSILON_UPPER_BOUND = 0.6;
FILEPATH = 'simulationFiles\deltaBound\knn\';
%% create xyz uniform disterbution
if nargin==2
    bounds = [1 1 1];
    dims = [1 1 1];
end
[xy,xz] = Utils.UniformDist(NUMBER_OF_POINTS_IN_DIST,bounds,dims);
%% run basic DM on x - phiX
[xGT] = DM_AD.Basic_DM( xy(1:dims(1),:),dims(1));
%% create 3-D function of AD reconstruction error compare to Phix
deltaZSim = zeros(EpsPoints,EpsPoints);
deltaYSim = zeros(EpsPoints,EpsPoints);
n1Sim = zeros(EpsPoints,EpsPoints);
n2Sim = zeros(EpsPoints,EpsPoints);
RdeltaZSim = zeros(EpsPoints,EpsPoints);
RdeltaYSim = zeros(EpsPoints,EpsPoints);
Rn1Sim = zeros(EpsPoints,EpsPoints);
Rn2Sim = zeros(EpsPoints,EpsPoints);
epss = linspace(EPSILON_LOWER_BOUND,EPSILON_UPPER_BOUND,EpsPoints);
% create sim struct par
par.xy = xy;
par.xz = xz;
par.bounds = bounds;
for kk=1:EpsPoints
    for ii=1:EpsPoints
        par.epsilons = [epss(kk),epss(ii)];
        par.ReverseOrder =1;
        par.dims = dims;
        [RdeltaZ,~,RdeltaY,~,Rn1,Rn2] = ADsim.sims_knn_kernels( 1,0,0,bounds,0,par);
        RdeltaZSim(kk,ii) =RdeltaZ;
        RdeltaYSim(kk,ii) =RdeltaY;
        Rn1Sim(kk,ii) =Rn1;
        Rn2Sim(kk,ii) =Rn2;
        par.ReverseOrder =0;
        [deltaZ,~,deltaY,~,n1,n2] = ADsim.sims_knn_kernels( 1,0,0,bounds,0,par);
        deltaZSim(kk,ii) =deltaZ;
        deltaYSim(kk,ii) =deltaY;
        n1Sim(kk,ii) =n1;
        n2Sim(kk,ii) =n2;
    end
end
save([(strcat(FILEPATH, 'new_RdeltaSim')),num2str(deltaSimNum)],'RdeltaZSim', 'RdeltaYSim' ,'xGT','xy','xz','epss','bounds','dims','Rn1Sim','Rn2Sim');
save([(strcat(FILEPATH, 'new_deltaSim')),num2str(deltaSimNum)],'deltaZSim', 'deltaYSim' ,'xGT','xy','xz','epss','bounds','dims','n1Sim','n2Sim');

end