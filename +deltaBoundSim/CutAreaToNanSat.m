function [ Osur ] = CutAreaToNanSat( sur,SatInd,area)
%put nan in saturated areas and return surface in unsaturated support
Osur = sur;
Osur(SatInd)=nan;
Osur = Osur(area(1):area(2),area(3):area(4));
end

