function [ ] = boundsOfSim(deltaSimNum,ReverseOrder)
%{
-Function name: boundsOfSim
-Description:
* the function calculates delta upper and lower bounds according to
* (2.1) (2.2) (1.1) (1.2) in formal problem, using simulation mat files.
-Inputs:
* deltaSimNum - number of simulation to use
* ReverseOrder - if true reverse order of AD kernel is taken
-Outputs:
* None
-Reurn values:
* plots delta bounds
-Notes:
* assumption c2= bounds(3) , c3= bounds(2)
%}
%% imports
import Utils.* deltaBoundSim.*
%% constants
% FILEPATH = 'D:\Users\Shay\Documents\ronen_repo\simulationFiles\deltaBound\old\';
FILEPATH = 'simulationFiles\deltaBound\knn\';
NUMBER_OF_POINTS=1000;
bounds=0;
% TODO: fix bounds in all files
%% code
if nargin==1
    ReverseOrder=0;
end
if(ReverseOrder)
    load([(strcat(FILEPATH, 'new_RdeltaSim')),num2str(deltaSimNum)]);
    deltaZSim =RdeltaZSim;
    deltaYSim =RdeltaYSim;
    n1Sim=Rn2Sim;
    n2Sim=Rn1Sim;
    [epss1,epss2] = meshgrid(epss);
else
    load([(strcat(FILEPATH, 'new_deltaSim')),num2str(deltaSimNum)]);
    [epss2,epss1] = meshgrid(epss);
end
%% calc upper bounds
UBdeltaY = 1-(epss1+epss2).^(dims(1)/dims(2));
UBdeltaZ = 1-(epss1).^(dims(1)/dims(3));
%% calc lower bounds
c = [std(xy(:)), std(xz(:))];
n1 = NUMBER_OF_POINTS.*epss1.^((dims(1)+dims(2)));
n2 = NUMBER_OF_POINTS^2*epss1.^((dims(1)+dims(2)))*epss2.^((dims(1)+dims(3)));% original 
% n1 = n1Sim;
% n2 = n2Sim;
alpha = c(2)*n1;
beta = c(1)*n2;
lb_deltaz = alpha.^(-1/(dims(3)+1));
lb_deltay = beta.^(-1/(dims(2)+1));
%% aditional computions for lower bound

LBdeltaYExact = lambertw(beta)./beta;
LBdeltaZExact = lambertw(alpha)./alpha;

%% edit data delta Y
% try
%     [USdeltaY, SatIndY,areaY ] = CutSaturationArea(deltaYSim);
%     UBdeltaY = CutAreaToNanSat(UBdeltaY,SatIndY,areaY);
%     LBdeltaYExact = CutAreaToNanSat(LBdeltaYExact,SatIndY,areaY);
%     lb_deltay = CutAreaToNanSat(lb_deltay,SatIndY,areaY);
%     LBdeltaYExAp = CutAreaToNanSat(LBdeltaYExAp,SatIndY,areaY);
%     epss1YUS = (epss1(areaY(1):areaY(2),areaY(3):areaY(4)));
%     epss2YUS = (epss2(areaY(1):areaY(2),areaY(3):areaY(4)));
% catch e
%     fprintf(1,'The identifier was:\n%s',e.identifier);
%     fprintf(1,'There was an error! The message was:\n%s',e.message);
    epss2YUS = epss2;
    epss1YUS = epss1; 
    USdeltaY = deltaYSim;
% end
%% plot for deltaY
surfacePlots(epss1YUS,epss2YUS,USdeltaY , UBdeltaY, LBdeltaYExact, lb_deltay)
set(gca, 'XScale', 'log', 'YScale', 'log');
legend('deltaY','upperBound','lower bound exact', 'used in paper LB');
xlabel('log(epsX)');
ylabel('log(epsY)');
if(~ReverseOrder)
    title(['\delta_2 ,bounds: ',num2str(bounds)]);
else
    title(['\delta_2 ,bounds: ',num2str(bounds),' Reverse Order Kernel']);
end
%% edit data
% try
%     [USdeltaZ, SatIndZ,areaZ ] = CutSaturationArea(deltaZSim);
%     UBdeltaZ= CutAreaToNanSat(UBdeltaZ,SatIndZ,areaZ);
%     LBdeltaZExact= CutAreaToNanSat(LBdeltaZExact,SatIndZ,areaZ);
%     lb_deltaz= CutAreaToNanSat(lb_deltaz,SatIndZ,areaZ);
%     LBdeltaZExAp = CutAreaToNanSat(LBdeltaZExAp,SatIndZ,areaZ);
%     epss1ZUS = (epss1(areaZ(1):areaZ(2),areaZ(3):areaZ(4)));
%     epss2ZUS = (epss2(areaZ(1):areaZ(2),areaZ(3):areaZ(4)));
% catch e
%     fprintf(1,'The identifier was:\n%s',e.identifier);
%     fprintf(1,'There was an error! The message was:\n%s',e.message);
    epss2ZUS = epss2;
    epss1ZUS = epss1; 
    USdeltaZ = deltaZSim;
% end
%% plot for deltaZ
surfacePlots(epss1ZUS,epss2ZUS,USdeltaZ , UBdeltaZ, LBdeltaZExact, lb_deltaz)
set(gca, 'XScale', 'log', 'YScale', 'log');
legend('deltaZ','upperBound','lower bound exact', 'used in paper LB');
xlabel('log(epsX)');
ylabel('log(epsY)');
if(~ReverseOrder)
    title(['\delta_1 ,bounds: ',num2str(bounds),' dims:', num2str(dims)]);
else
    title(['\delta_1 ,bounds: ',num2str(bounds),' dims:', num2str(dims),' Reverse Order Kernel']);
end

end

function [lower_bound_delta_z , lower_bound_delta_y] =calcLowerBounds(bounds,n1,n2,dims)
    alpha = bounds(3)*n1;
    beta = bounds(2)*n2;
    lower_bound_delta_z = alpha.^(-1/(dims(2)+1));
    lower_bound_delta_y = beta.^(-1/(dims(3)+1));
end

