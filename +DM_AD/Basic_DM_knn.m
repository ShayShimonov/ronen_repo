function [out_rep, S] = Basic_DM_knn(samps,dim)
%BASIC_DM_KNN Summary of this function goes here
%   Detailed explanation goes here
import Utils.*
%NOTE: sum of coloums =1 
[~,N] = size(samps);
samps = samps';
K_init = squareform( pdist(samps) );
kernel_scale = median(K_init(:));
kernel_scale = Utils.checkEps(kernel_scale,K_init,1);
k = max(floor(N* kernel_scale^2),1);
%note the +1 is because the distance to self is counted
knn_mask = pdist2knn_mask(K_init,k+1);
W = exp(-K_init.^2/kernel_scale^2);
W = W.*knn_mask;
w_inv1 = diag((1./sum(W)));
Knn = W*w_inv1;
[~,S,V] = svd(Knn);
out_rep = (V(:,2:dim+1))';
end

