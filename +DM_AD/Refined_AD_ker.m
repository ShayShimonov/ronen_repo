function [kernel_AD, eps1, eps2] = Refined_AD_ker(samps1,samps2,kernelType, rec_dim)
% this function preforms Refined AD and returns joined kernel
% samps - data samples of each process
% recDim - dimention of wished common variable reconstuction
% kernelType - type of kernel can be: 'regular','symetric','anti symetric' or
% 'all'
% kernel_AD - joined kernel
import Utils.* DM_AD.*
vStd = [std(samps1(:)), std(samps2(:))];
dSig1 = Utils.dim_ev(samps1);
dSig2 = Utils.dim_ev(samps2);
n = size(samps1,2);
if nargin ==3
    [reg_com] = DM_AD.Basic_AD_knn(samps1,samps2,1,[0 0],0,'symmetric');
%     s_reg = eig(reg_com);
%     [~,dim_reg] = min(abs(s_reg-0.5*(s_reg(1))));
    recDim = dim_ev(reg_com);
else
    recDim = rec_dim; 
end
if (dSig1>recDim)
    disp("Warning: dSig1 is" + num2str(dSig1) + " while recDim is " + num2str(recDim));
end
if (dSig2>recDim)
    disp("Warning: dSig2 is" + num2str(dSig2) + " while recDim is " + num2str(recDim));
end
if(vStd(1)>=vStd(2))
     c = vStd;
     dims = [recDim,abs(dSig1-recDim),abs(dSig2-recDim)];
else
     c = flip(vStd);
     dims = [recDim,abs(dSig2-recDim),abs(dSig1-recDim)];
end   
[eps1,eps2] = Utils.EpsAlg(n,c,dims);
[kernel_AD] = DM_AD.Basic_AD_knn(samps1,samps2,-1,[eps1,eps2],(vStd(1)<vStd(2)),kernelType);  
end

