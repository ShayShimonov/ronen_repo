function [ Pcom, eps1, eps2] = Basic_AD_knn( samps1 , samps2,k,epsilons,ReverseOrder,type )
% function [ Pcom] = Basic_AD_knn( samps1 , samps2, dim,k,epsilons,ReverseOrder )
% k- if positive epsilons of process are median times k
% if negitive epsilons of process are ditermine by epsilons
% ReverseOrder - if true reverse order of AD kernel is taken
% EigsMaxDiffInd - index of maximum differance eigen value (assumed to be
% the information holder dim band);
import Utils.* DM_AD.*
c1r2 = 1;%if c1r2=1 sum of coloums =1, if c1r2=2 sum of rows =1 
if nargin ==5
    ReverseOrder=0;
end
if k>0
    K1 = squareform( pdist(samps1') );
    eps1 = k*median(K1(:));
    eps1 = Utils.checkEps(eps1,K1,k);
else
    eps1 = epsilons(1);
end
P1 = Utils.knn_kernel(samps1,eps1);

if k>0
    K2 = squareform( pdist(samps2') );
    eps2 = k*median(K2(:));
    eps2 = Utils.checkEps(eps2,K2);
else
    eps2 = epsilons(2);
end
P2 = Utils.knn_kernel(samps2,eps2);
if(ReverseOrder)
    %     P = P1*P2;  %AD kernel reverse order
    Pa = P1;
    Pb=P2;
else
    Pa = P2;
    Pb=P1;
    %     P = P2*P1; %AD kernel
end
switch type
    case 'regular'
        P = Pa*Pb;
    case 'symmetric'
        P = Pa*Pb'+Pb'*Pa;
    case 'anti symmetric'
        P = Pa*Pb'-Pb'*Pa;
    otherwise
        error('type must be: regular,symmetric or anti symmetric');
end
% [~,~,V] = svd(P);
% common = (V(:,2:dim+1))';
Kcom = squareform( pdist(P') );
epscom = median(Kcom(:));
Wcom = exp(-Kcom.^2/epscom^2);
w_inv_com = diag((1./sum(Wcom)));
Pcom = Wcom*w_inv_com;

% [~,~,V] = svd(Pcom);
% common = (V(:,2:dim+1))';
end
