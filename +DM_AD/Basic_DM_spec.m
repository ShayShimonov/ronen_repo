function [out_rep,Sd] = Basic_DM_spec( samps,dim,m,bounds,epsilon)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%NOTE: sum of coloums =1 
import Utils.* DM_AD.* 
% K = CyclicPdist( samps,bounds );
K = squareform( pdist(samps') );
if m>0
    epss = m*median(K(:));
    epss = Utils.checkEps(epss,K,m);
else
    epss = epsilon;
end
W = exp(-K.^2/epss^2);
w_inv = diag((1./sum(W)));
P = W*w_inv;
[~,S,V] = svd(P);
out_rep = (V(:,2:dim+1))';
Sd =diag(S);
Sd = Sd(2:end);
end
