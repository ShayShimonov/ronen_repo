function [ common] = Basic_AD_mod( samps1 , samps2, dim,k,epsilons,ReverseOrder )
%UNTITLED Summary of this function goes here
%   k- if positive epsilons of process are median times k
% if negitive epsilons of process are ditermine by epsilons
% ReverseOrder - if true reverse order of AD kernel is taken
% EigsMaxDiffInd - index of maximum differance eigen value (assumed to be
% the information holder dim band);
import Utils.* DM_AD.*
%NOTE: sum of coloums =1 
if nargin ==5
    ReverseOrder=0;
end
K1 = squareform( pdist(samps1') );
if k>0
    eps1 = k*median(K1(:));
else
    eps1 = epsilons(1);
end
W1 = exp(-K1.^2/eps1^2);
w_inv1 = diag((1./sum(W1)));
P1 = W1*w_inv1;
K2 = squareform( pdist(samps2') );
if k>0
    eps2 = k*median(K2(:));
else
    eps2 = epsilons(2);
end
W2 = exp(-K2.^2/eps2^2);
w_inv2 = diag((1./sum(W2)));
P2 = W2*w_inv2;
if(ReverseOrder)
    P = P1*P2;  %AD kernel reverse order
else
    P = P2*P1; %AD kernel
end
% [~,~,V] = svd(P);
% common = (V(:,2:dim+1))';
Kcom = squareform( pdist(P') );
epscom = median(Kcom(:));
Wcom = exp(-Kcom.^2/epscom^2);
w_inv_com = diag((1./sum(Wcom)));
Pcom = Wcom*w_inv_com;
[~,~,V] = svd(Pcom);
common = (V(:,2:dim+1))';
end

