function [C,S,names] = RefinedAD(samps1,samps2,recDim,kernelType)
% this function preforms Refined AD 
% samps - data samples of each process
% recDim - dimention of wished common variable reconstuction
% kernelType - type of kernel can be: 'regular','symetric','anti symetric' or
% 'all'
% C- full representation matrix of AD
% S - full spectrum of AD
% names - orderd names of kernels incase of all, o.w equals to kernelType
import Utils.* DM_AD.*
% allSig = [samps1(:),samps2(:)];
vStd = [std(samps1(:)), std(samps2(:))];
n = size(samps1,2);
dSig1 = dim_ev(samps1);
dSig2 = dim_ev(samps2);
if(vStd(1)>=vStd(2))
     c = vStd;
     dims = [recDim,dSig1-recDim,dSig2-recDim];
else
     c = flip(vStd);
     dims = [recDim,dSig2-recDim,dSig1-recDim];
end   
[eps1,eps2] = EpsAlg(n,c,dims);
if(strcmp(kernelType,'all'))
    names = {'regular','symmetric','anti symmetric'};
    for ii=1:3
      [C{ii},S{ii}] = Basic_AD_spec(samps1,samps2,0,-1,[eps1,eps2],(vStd(1)<vStd(2)),names{ii}); 
    end
else
    [C,S] =  Basic_AD_spec(samps1,samps2,0,-1,[eps1,eps2],(vStd(1)<vStd(2)),kernelType);
    names = kernelType;
end  
end

