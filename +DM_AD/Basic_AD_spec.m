function [ common,Sd] = Basic_AD_spec( samps1 , samps2, dim,k,epsilons,ReverseOrder,type)
%UNTITLED Summary of this function goes here
%   k- if positive epsilons of process are median times k
% if negitive epsilons of process are ditermine by epsilons
% ReverseOrder - if true reverse order of AD kernel is taken
% type of kernel - 'regular', 'symetric' , 'anti symetric'
import Utils.* DM_AD.*
%NOTE: sum of coloums =1 
if nargin ==5
    ReverseOrder=0;
end
K1 = squareform( pdist(samps1') );
if k>0
    eps1 = k*median(K1(:));
    eps1 = Utils.checkEps(eps1,K1,k);
else
    eps1 = epsilons(1);
end
W1 = exp(-K1.^2/eps1^2);
w_inv1 = diag((1./sum(W1)));
P1 = W1*w_inv1;
K2 = squareform( pdist(samps2') );
if k>0
    eps2 = k*median(K2(:));
    eps2 = Utils.checkEps(eps2,K2,k);
else
    eps2 = epsilons(2);
end
W2 = exp(-K2.^2/eps2^2);
w_inv2 = diag((1./sum(W2)));
P2 = W2*w_inv2;
if(ReverseOrder)
    %     P = P1*P2;  %AD kernel reverse order
    Pa = P1;
    Pb=P2;
else
    Pa = P2;
    Pb=P1;
    %     P = P2*P1; %AD kernel
end
switch type
    case 'regular'
        P = Pa*Pb;
    case 'symmetric'
        P = Pa*Pb'+Pb'*Pa;
    case 'anti symmetric'
        P = Pa*Pb'-Pb'*Pa;
    otherwise
        error('type must be: regular,symmetric or anti symmetric');
end
% [~,~,V] = svd(P);
% common = (V(:,2:dim+1))';
Kcom = squareform( pdist(P') );
epscom = median(Kcom(:));
Wcom = exp(-Kcom.^2/epscom^2);
w_inv_com = diag((1./sum(Wcom)));
Pcom = Wcom*w_inv_com;
[~,S,V] = svd(Pcom);
if(dim>0)
    if(numel(dim)==1)
        common = (V(:,2:dim+1))';
    else
        for ii=1:(numel(dim))
            common{ii} = (V(:,2:dim(ii)+1))';
        end
    end
else
    common= (V(:,2:end))';
end
Sd = diag(S);
Sd = Sd(2:end);
end

