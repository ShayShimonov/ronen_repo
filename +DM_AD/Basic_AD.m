function [ common,P ] = Basic_AD( samps1 , samps2, dim,k )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% smaples are supose to be in the columbs
if nargin==3    
    k=1;
end
%NOTE: sum of coloums =1 
K1 = squareform( pdist(samps1') );
eps1 = k*median(K1(:));
W1 = exp(-K1.^2/eps1^2);
w_inv1 = diag((1./sum(W1)));
P1 = W1*w_inv1;
K2 = squareform( pdist(samps2') );
eps2 = k*median(K2(:));
W2 = exp(-K2.^2/eps2^2);
w_inv2 = diag((1./sum(W2)));
P2 = W2*w_inv2;
P = P2*P1; %AD kernel
[~,~,V] = svd(P);
common = (V(:,2:dim+1))';

end

