function [P ,kernel_scale] = Basic_DM_knn_mod( samps,m,bounds,epsilon, dims)
%This function uses cyclic Pdist instead of regular euclidic pdist
import Utils.* DM_AD.* 
%NOTE: sum of coloums =1 
K = CyclicPdist( samps,bounds );
[~,N] = size(samps);
assert(size(samps,1)<size(samps,2));
if m>0
    kernel_scale = m*median(K(:));
else
    kernel_scale = epsilon;
end
k = min(max(floor(N* kernel_scale^2),1),N);
%note the +1 is because the distance to self is counted
knn_mask = Utils.pdist2knn_mask(K,k+1);
W = exp(-K.^2/kernel_scale^2);
W = W.*knn_mask;
w_inv = diag((1./sum(W)));
P = W*w_inv;
end
