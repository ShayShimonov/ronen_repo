function [out_rep] = Basic_DM(samps,dim)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%NOTE: sum of coloums =1 
K = squareform( pdist(samps') );
kernel_scale = median(K(:));
W = exp(-K.^2/kernel_scale^2);
w_inv = diag((1./sum(W)));
P = W*w_inv;
[~,~,V] = svd(P);
out_rep = (V(:,2:dim+1))';
end

