function [P ,epss] = Basic_DM_mod( samps,m,bounds,epsilon)
%This function uses cyclic Pdist instead of regular euclidic pdist
import Utils.* DM_AD.* 
%NOTE: sum of coloums =1 
K = CyclicPdist( samps,bounds );
if m>0
    epss = m*median(K(:));
else
    epss = epsilon;
end
W = exp(-K.^2/epss^2);
w_inv = diag((1./sum(W)));
P = W*w_inv;
end

