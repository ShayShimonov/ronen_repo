function [ same_hand,different_hand ] = segDivideOx( fname,dur,fs,Newfor )
%UNTITLED3 Summary of this function goes here
% fs- sampling rate
% dur - duration of each segment in seconds
% fname - file name
% same_hand - 3d array of segments of signal for each oximeter
import Utils.*
if nargin==3 
    Newfor = 1;
end
% fix signals before process
    if(~Newfor)
    a= load(fname);
    [ox1SH_signal,ox2SH_signal]=syncOxSig(a.ox1SH,a.locs1SH,a.ox2SH,a.locs2SH);
    [ox1DH_signal,ox2DH_signal]=syncOxSig(a.ox1DH,a.locs1DH,a.ox2DH,a.locs2DH);
    [ox1SH_signal,ox1DH_signal] = CutbyShort(ox1SH_signal,ox1DH_signal);
    [ox2SH_signal,ox2DH_signal] = CutbyShort(ox2SH_signal,ox2DH_signal);
    else
        load('+OximeterEx\experiments_11_13.mat');
        [ox1SH_signal,ox2SH_signal]=syncOxSig(exps(1).oxSig1_Raw, exps(1).oxLocs1, exps(1).oxSig2_Raw,exps(1).oxLocs2);
        [ox1DH_signal,ox2DH_signal]=syncOxSig(exps(2).oxSig1_Raw, exps(2).oxLocs1, exps(2).oxSig2_Raw,exps(2).oxLocs2);
        [ox1SH_signal,ox1DH_signal] = CutbyShort(ox1SH_signal,ox1DH_signal);
        [ox2SH_signal,ox2DH_signal] = CutbyShort(ox2SH_signal,ox2DH_signal);
    end       
    % divide into 10 sec segments
    dsec = dur*fs;
    shortSegTimes = dsec:dsec:length(ox2SH_signal);
    Nseg = length(shortSegTimes);
    same_hand= zeros(dsec,Nseg,2);
    different_hand= zeros(dsec,Nseg,2);
    for kk=1:Nseg
        same_hand(:,kk,1) = ox1SH_signal((1+(kk-1)*dsec):(kk*dsec));
        same_hand(:,kk,2) = ox2SH_signal((1+(kk-1)*dsec):(kk*dsec));
        different_hand(:,kk,1) = ox1DH_signal((1+(kk-1)*dsec):(kk*dsec));
        different_hand(:,kk,2) = ox2DH_signal((1+(kk-1)*dsec):(kk*dsec));
    end
end

