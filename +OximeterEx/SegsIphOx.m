function [same_hand,different_hand] = SegsIphOx(dur,Newfor)
%UNTITLED3 Summary of this function goes here
% fs- sampling rate
% dur - duration of each segment in seconds
% fname - file name
% same_hand - 3d array of segments of signal for each oximeter
import Utils.*
% fix signals before process
if nargin==3
    Newfor = 1;
end
if(~Newfor)
    fs_ox =500; fs_ip = 29.86;
    same_hand= [];
    different_hand= [];
    for nn=1:4
        load(['D:\Users\Shay\Documents\sipl_repo\Main\exp',num2str(nn),'.mat']);
        eval(['ipSig = ipSigE',num2str(nn),';']);eval(['ipLocs = ipLocsE',num2str(nn),';']);
        eval(['oxSig1 = oxSig1E',num2str(nn),';']);eval(['oxLocs1 = oxLocs1E',num2str(nn),';']);
        eval(['oxSig2 = oxSig2E',num2str(nn),';']);eval(['oxLocs2 = oxLocs2E',num2str(nn),';']);
        [oxSig1,oxSig2]=syncOxSig(oxSig1,oxLocs1,oxSig2,oxLocs2);
        ipSig = ipSig(ipLocs(1):end);
        oxSig1 = downsample(oxSig1,round(fs_ox/ fs_ip));
        oxSig2 = downsample(oxSig2,round(fs_ox/ fs_ip));
        [oxSig1,ipSig] = CutbyShort(oxSig1,ipSig);
        [oxSig2,ipSig] = CutbyShort(oxSig2,ipSig);
        % normelize data
        oxSig1 = normalize01(oxSig1);
        oxSig2 = normalize01(oxSig2);
        ipSig = normalize01(ipSig);
        % divide into 10 sec segments
        dsec = round(dur*fs_ip);
        SegTimes = dsec:dsec:length(oxSig2);
        Nseg = length(SegTimes);
        Curr_hand= zeros(dsec,Nseg,3);
        for kk=1:Nseg
            Curr_hand(:,kk,1) = oxSig1((1+(kk-1)*dsec):(kk*dsec));
            Curr_hand(:,kk,2) = oxSig2((1+(kk-1)*dsec):(kk*dsec));
            Curr_hand(:,kk,3) = ipSig((1+(kk-1)*dsec):(kk*dsec));
        end
        if(mod(nn,2)==1)
            same_hand = [same_hand,Curr_hand];
        else
            different_hand = [different_hand,Curr_hand];
        end
    end
else   
    load('+OximeterEx\experiments_11_13.mat');
    for ii=1:2
    fs_ox =500; fs_ip = exps(ii).iphoneSR;
    [oxSig1,oxSig2]=syncOxSig(exps(ii).oxSig1_Raw, exps(ii).oxLocs1, exps(ii).oxSig2_Raw,exps(ii).oxLocs2);
        ipSig = exps(ii).ipSig(exps(ii).ipLocs(1):end);
        oxSig1 = downsample(oxSig1,round(fs_ox/ fs_ip));
        oxSig2 = downsample(oxSig2,round(fs_ox/ fs_ip));
        [oxSig1,ipSig] = CutbyShort(oxSig1,ipSig);
        [oxSig2,ipSig] = CutbyShort(oxSig2,ipSig);
        % normelize data
        oxSig1 = normalize01(oxSig1);
        oxSig2 = normalize01(oxSig2);
        ipSig = normalize01(ipSig);
        % divide into 10 sec segments
        dsec = round(dur*fs_ip);
        SegTimes = dsec:dsec:length(oxSig2);
        Nseg = length(SegTimes);
        Curr_hand= zeros(dsec,Nseg,3);
        for kk=1:Nseg
            Curr_hand(:,kk,1) = oxSig1((1+(kk-1)*dsec):(kk*dsec));
            Curr_hand(:,kk,2) = oxSig2((1+(kk-1)*dsec):(kk*dsec));
            Curr_hand(:,kk,3) = ipSig((1+(kk-1)*dsec):(kk*dsec));
        end
        if(mod(ii,2)==1)
            same_hand = Curr_hand;
        else
            different_hand = Curr_hand;
        end
    end
    
end
end

