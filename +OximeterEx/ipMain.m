% this script provides the experiment compared with the iphone signal
% it has a few minor unfixed problems 
%% with iphone
clear all;
import Utils.* DM_AD.* OximeterEx.*
dur=1/8;
% [same_hand,different_hand] = SegsIphOx(dur,0);
[same_hand,different_hand] = SegsIphOx(dur,1);
 % calulate std of processes
 ox1_SH =same_hand(:,:,1);
 ox2_SH =same_hand(:,:,2);
 ip_SH =same_hand(:,:,3);
 all_Sig_SH = [ox1_SH(:),ox2_SH(:),ip_SH(:)];
 cSH= std(all_Sig_SH);
 ox1_DH =different_hand(:,:,1);
 ox2_DH =different_hand(:,:,2);
 ip_DH =different_hand(:,:,3);
 all_Sig_DH = [ox1_DH(:),ox2_DH(:),ip_DH(:)];
 cDH= std(all_Sig_DH);
%preform AD 
[ComSH_ox,S_SH_ox] = Basic_AD_spec(ox1_SH,ox2_SH,0,0.1,[0.1 0.1],double(cSH(2)>cSH(1)),'regular');
[ComDH_ox,S_DH_ox] = Basic_AD_spec(ox1_DH,ox2_DH,0,0.1,[0.1 0.1],double(cDH(2)>cDH(1)),'regular');
[ComSH_ip,S_SH_ip] = Basic_AD_spec(ox1_SH,ip_SH,0,0.1,[0.1 0.1],double(cSH(2)>cSH(1)),'regular');
[ComDH_ip,S_DH_ip] = Basic_AD_spec(ox1_DH,ip_DH,0,0.1,[0.1 0.1],double(cDH(2)>cDH(1)),'regular');
%display
figure()
plot(S_SH_ox);
hold on;
plot(S_SH_ip);
% hold off;
% title('spectrum comparison bertween oximeter and iphone for same hand');
% ylabel('spectrum');
% xlabel('dim');
% legend('oxmeter','iphone');
% figure()
plot(S_DH_ox);
% hold on;
plot(S_DH_ip);
hold off;
% title('spectrum comparison bertween oximeter and iphone for different hand');
title('regular spectrum comparison bertween oximeter and iphone');
ylabel('spectrum');
xlabel('dim');
legend('ox-ox same hand','ip-ox same hand','ox-ox diff hand','ip-ox diff hand');
%preform AD with A kernel
[ComSH_ox_A,S_SH_ox_A] = Basic_AD_spec(same_hand(:,:,1),same_hand(:,:,2),0,0.1,[0.1 0.1],double(cSH(2)>cSH(1)),'anti symmetric');
[ComDH_ox_A,S_DH_ox_A] = Basic_AD_spec(different_hand(:,:,1),different_hand(:,:,2),0,0.1,[0.1 0.1],double(cDH(2)>cDH(1)),'anti symmetric');
[ComSH_ip_A,S_SH_ip_A] = Basic_AD_spec(same_hand(:,:,1),same_hand(:,:,3),0,0.1,[0.1 0.1],double(cSH(3)>cSH(1)),'anti symmetric');
[ComDH_ip_A,S_DH_ip_A] = Basic_AD_spec(different_hand(:,:,1),different_hand(:,:,3),0,0.1,[0.1 0.1],double(cDH(3)>cDH(1)),'anti symmetric');
%display
figure()
plot(S_SH_ox_A);
hold on;
plot(S_SH_ip_A);
% hold off;
% title('spectrum comparison bertween oximeter and iphone for same hand anti symmetric kernel');
% ylabel('spectrum');
% xlabel('dim');
% legend('oxmeter','iphone');
% figure()
plot(S_DH_ox_A);
% hold on;
plot(S_DH_ip_A);
hold off;
% title('spectrum comparison bertween oximeter and iphone for different hand anti symmetric kernel');
title('spectrum comparison bertween oximeter and iphone - anti symmetric kernel');
ylabel('spectrum');
xlabel('dim');
% legend('oxmeter','iphone');
legend('ox-ox same hand','ip-ox same hand','ox-ox diff hand','ip-ox diff hand');
%preform AD with S kernel
[ComSH_ox_S,S_SH_ox_S] = Basic_AD_spec(same_hand(:,:,1),same_hand(:,:,2),0,0.1,[0.1 0.1],double(cSH(2)>cSH(1)),'symmetric');
[ComDH_ox_S,S_DH_ox_S] = Basic_AD_spec(different_hand(:,:,1),different_hand(:,:,2),0,0.1,[0.1 0.1],double(cDH(2)>cDH(1)),'symmetric');
[ComSH_ip_S,S_SH_ip_S] = Basic_AD_spec(same_hand(:,:,1),same_hand(:,:,3),0,0.1,[0.1 0.1],double(cSH(2)>cSH(1)),'symmetric');
[ComDH_ip_S,S_DH_ip_S] = Basic_AD_spec(different_hand(:,:,1),different_hand(:,:,3),0,0.1,[0.1 0.1],double(cDH(2)>cDH(1)),'symmetric');
%display
figure()
plot(S_SH_ox_S);
hold on;
plot(S_SH_ip_A);
% hold off;
% title('spectrum comparison bertween oximeter and iphone for same hand symmetric kernel');
% ylabel('spectrum');
% xlabel('dim');
% legend('oxmeter','iphone');
% figure()
plot(S_DH_ox_A);
% hold on;
plot(S_DH_ip_A);
hold off;
% title('spectrum comparison bertween oximeter and iphone for different hand symmetric kernel');
title('spectrum comparison bertween oximeter and iphone - symmetric kernel');
ylabel('spectrum');
xlabel('dim');
% legend('oxmeter','iphone');
legend('ox-ox same hand','ip-ox same hand','ox-ox diff hand','ip-ox diff hand');
%% try with our algorithem 
[~,S_SH_oxox,names] = RefinedAD(ox1_SH,ox2_SH,1,'all');
[~,S_DH_oxox,~] = RefinedAD(ox1_DH,ox2_DH,1,'all');
[~,S_SH_oxip,~] = RefinedAD(ox1_SH,ip_SH,1,'all');
[~,S_DH_oxip,~] = RefinedAD(ox1_DH,ip_DH,1,'all');
for kk=1:3
    figure()
    plot(S_SH_oxox{kk});
    hold on;
%     plot(S_SH_oxip{kk});
    plot(S_DH_oxox{kk});
%     plot(S_DH_oxip{kk});
    hold off;
    title(['Refined AD spectrum comparison - ',names{kk}]);
    ylabel('spectrum');
    xlabel('dim');
%     l = legend('ox-ox same hand','ip-ox same hand','ox-ox diff hand','ip-ox diff hand');
%     l.FontSize =20;
legend('same hand','different hand');
end
