% this script provides the figures in chapter 3.1.3 - namely oximeter
% expirements
clear all;
Newform =1;% if true the long full and latest experiment is used o.w old ones are used
import Utils.* DM_AD.* OximeterEx.*
% fix signals before process and divide into 10 sec segments
fs =500; dur=1/5;
% [ same_handL,different_handL ] = segDivideOx( '+OximeterEx\LongExp.mat',dur,fs,0 );
% [ same_handS,different_handS ] = segDivideOx('+OximeterEx\ShortExp.mat',dur,fs,0 );
if(~Newform)
    [ same_handL,different_handL ] = segDivideOx( '+OximeterEx',dur,fs,Newform );
    [ same_handS,different_handS ] = segDivideOx('+OximeterEx',dur,fs,Newform );
    same_hand(:,:,1) = [ same_handS(:,:,1),same_handL(:,:,1)];
    same_hand(:,:,2) = [ same_handS(:,:,2),same_handL(:,:,2)];
    different_hand(:,:,1) = [different_handS(:,:,1),different_handL(:,:,1)];
    different_hand(:,:,2) = [different_handS(:,:,2),different_handL(:,:,2)];
else
    [ same_hand,different_hand ] = segDivideOx( '+OximeterEx',dur,fs,Newform );
end
Nseg = size(same_hand,2);
%normalize data
sigSH = same_hand;sigDH = different_hand;
sigSH = sigSH-min(sigSH);
sigDH = sigDH-min(sigDH);
sigSH = sigSH./max(sigSH);
sigDH = sigDH./max(sigDH);
% sigSH = normalize01(sigSH);
% sigDH = normalize01(sigDH);
time = (1/fs:1/fs:dur);
% calulate std of processes
a_SH =sigSH(:,:,1);
b_SH =sigSH(:,:,2);
all_Sig_SH = [a_SH(:),b_SH(:)];
cSH= std(all_Sig_SH);
a_DH =sigDH(:,:,1);
b_DH =sigDH(:,:,2);
all_Sig_DH = [a_DH(:),b_DH(:)];
cDH= std(all_Sig_DH);
%preform AD with dim=1,2,refDim
[commonCurrSH,SCurrSH] = Basic_AD_spec(sigSH(:,:,1),sigSH(:,:,2),0,0.1,[0.1 0.1],double(cSH(1)>cSH(2)),'regular');
[commonCurrDH,SCurrDH] = Basic_AD_spec(sigDH(:,:,1),sigDH(:,:,2),0,0.1,[0.1 0.1],double(cDH(1)>cDH(2)),'regular');
%display
figure()
plot(SCurrSH,'LineWidth',2);
hold on;
plot(SCurrDH,'LineWidth',2);
hold off;
title('spectrum comparison bertween hands Naive AD - regular Kernel','FontSize',12);
ylabel('spectrum');
xlabel('dimention');
l= legend('same hand','different hand');
l.FontSize = 16;
 xlim([1 500]);
%preform AD with dim=1,2,refDim
[commonCurrSH_A,SCurrSH_A] = Basic_AD_spec(sigSH(:,:,1),sigSH(:,:,2),0,0.1,[0.1 0.1],double(cSH(1)>cSH(2)),'anti symmetric');
[commonCurrDH_A,SCurrDH_A] = Basic_AD_spec(sigDH(:,:,1),sigDH(:,:,2),0,0.1,[0.1 0.1],double(cDH(1)>cDH(2)),'anti symmetric');
%display
figure()
plot(SCurrSH_A,'LineWidth',2);
hold on;
plot(SCurrDH_A,'LineWidth',2);
hold off;
title('spectrum comparison bertween hands Naive AD -anti Symmetric Kernel','FontSize',12);
ylabel('spectrum');
xlabel('dimention');
l= legend('same hand','different hand');
l.FontSize = 16;
 xlim([1 500]);
%preform AD with dim=1,2,refDim
[commonCurrSH_S,SCurrSH_S] = Basic_AD_spec(sigSH(:,:,1),sigSH(:,:,2),0,0.1,[0.1 0.1],double(cSH(1)>cSH(2)),'symmetric');
[commonCurrDH_S,SCurrDH_S] = Basic_AD_spec(sigDH(:,:,1),sigDH(:,:,2),0,0.1,[0.1 0.1],double(cDH(1)>cDH(2)),'symmetric');
%display
figure()
plot(SCurrSH_S,'LineWidth',2);
hold on;
plot(SCurrDH_S,'LineWidth',2);
hold off;
title('spectrum comparison bertween hands Naive AD - Symmetric Kernel','FontSize',12);
ylabel('spectrum');
xlabel('dimention');
l = legend('same hand','different hand');
l.FontSize = 16;
 xlim([1 500]);
%% try with our values
[~,S_SH,~] = RefinedAD(sigSH(:,:,1),sigSH(:,:,2),1,'all');
%% try with our values diff hand

[~,S_DH,names] = RefinedAD(sigDH(:,:,1),sigDH(:,:,2),1,'all');
for kk=1:3
    figure()
    plot(1:length(S_SH{kk}),S_SH{kk},'LineWidth',2);
    hold on;
    plot(1:length(S_DH{kk}),S_DH{kk},'LineWidth',2);
    hold off;
    title(['spectrum comparison between hands Refined AD -',names{kk},...
        ' Kernel'],'FontSize',12);
    ylabel('spectrum');
    xlabel('dimention');
    l = legend('same hand','different hand');
     xlim([1 100]);
     l.FontSize = 16;
end

