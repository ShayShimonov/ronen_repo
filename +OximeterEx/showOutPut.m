% create
sh=load('+OximeterEx\Short_same hand.mat');
dh = load('+OximeterEx\Short_different hand.mat');
figure()
suptitle('1 dim rec vs time');
subplot(1,2,1);
plot(sh.samps1(1,:),sh.common{1});
xlabel('time[s]');
ylabel('reconstruction \phi_1');
title('same hand');
subplot(1,2,2);
plot(dh.samps1(1,:),dh.common{1});
xlabel('time[s]');
ylabel('reconstruction \phi_1');
title('differnt hand');
figure()
suptitle('2 dim rec');
subplot(1,2,1);
plot(sh.common{2}(1,:),sh.common{2}(2,:));
xlabel('reconstruction \phi_1');
ylabel('reconstruction \phi_2');
title('same hand');
subplot(1,2,2);
plot(dh.common{2}(1,:),dh.common{2}(2,:));
xlabel('reconstruction \phi_1');
ylabel('reconstruction \phi_2');
title('differnt hand');