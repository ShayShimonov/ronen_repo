%% sec 3
import Utils.* DM_AD.* Basics.*
% create 2 toruses with common angle and operate AD
N = 1000;
teta1 = rand(1,N)*2*pi;
teta2 = rand(1,N)*2*pi;
teta3 = rand(1,N)*2*pi;
R1 = 10;
R2 = 20;
r1 = 2;
r2 = 5;
samps1 = [(R1+r1*cos(teta2)).*cos(teta1);(R1+r1*cos(teta2)).*sin(teta1);r1*sin(teta2)];
samps2 = [(R2+r2*cos(teta3)).*cos(teta1);(R2+r2*cos(teta3)).*sin(teta1);r2*sin(teta3)];
figure(7);
counter = 1;
for k=0.0001:0.01:0.1
    [ common ] = Basic_AD( samps1 , samps2, 2,k );
    subplot(2,5,counter )
    scatter(common(1,:),common(2,:),10, teta1, 'Fill'); axis equal;
    title(['k=',num2str(k)]);
    counter =counter+1;
end
figure(8);
counter = 1;
for k=0.1:0.1:1
    [ common ] = Basic_AD( samps1 , samps2, 2,k );
    subplot(2,5,counter )
    scatter(common(1,:),common(2,:),10, teta1, 'Fill'); axis equal;
    title(['k=',num2str(k)]);
    counter =counter+1;
end
figure(9);
counter = 1;
for k=1:1:10
    [ common ] = Basic_AD( samps1 , samps2, 2,k );
    subplot(2,5,counter )
    scatter(common(1,:),common(2,:),10, teta1, 'Fill'); axis equal;
    title(['k=',num2str(k)]);
    counter =counter+1;
end
figure(10);
counter = 1;
for k=10:10:100
    [ common ] = Basic_AD( samps1 , samps2, 2,k );
    subplot(2,5,counter )
    scatter(common(1,:),common(2,:),10, teta1, 'Fill'); axis equal;
    title(['k=',num2str(k)]);
    counter =counter+1;
end
% figure(5);
% scatter3(torus2(1,:),torus2(2,:),torus2(3,:), 100, teta1, 'Fill'); axis equal;
% figure(6);
% scatter3(torus1(1,:),torus1(2,:),torus1(3,:), 100, teta1, 'Fill'); axis equal;
% common  = Basic_AD( torus1 , torus2,2 );
% figure(7);
