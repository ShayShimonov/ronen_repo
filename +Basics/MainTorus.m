close all;
clear;
import Utils.* DM_AD.* Basics.*

N = 1000;

vTh1 = rand(1, N) * 2 * pi;
vTh2 = rand(1, N) * 2 * pi;
vTh3 = rand(1, N) * 2 * pi;

% vTh1 = sort(vTh1);

R = 10;
r = 2;

f = @(mZ) [(R + r * cos(mZ(2,:))) .* cos(mZ(1,:));
           (R + r * cos(mZ(2,:))) .* sin(mZ(1,:))
            r * sin(mZ(2,:))];
        
mX = f([vTh1; vTh2; vTh3]);

% figure; scatter(1 : N, vTh1);
figure; scatter3(mX(1,:), mX(2,:), mX(3,:), 100, vTh1, 'Fill'); axis equal;

mW  = squareform( pdist(mX') );
eps = 1 * median(mW(:));
mK  = exp(-mW.^2 / eps^2);

mL  = bsxfun(@minus, mK, sum(mK, 2)); %mL = mK - vD;

[mPhi, mLam] = eig(mL);

figure; scatter3(mPhi(:,2), mPhi(:,3), mPhi(:,4), 100, vTh1, 'Fill'); axis equal;

figure; 
for ii = 1 : 9
    subplot(3,3,ii); scatter(vTh1, mPhi(:,ii+1));
end

% figure; imagesc(mW); colorbar;

