import Utils.* DM_AD.* Basics.*
N = 1000;
teta1 = rand(1,N)*2*pi;
teta2 = rand(1,N)*2*pi;
teta3 = rand(1,N)*2*pi;
R1 = 10;
R2 = 20;
r1 = 2;
r2 = 5;
samps1 = [(R1+r1*cos(teta2)).*cos(teta1);(R1+r1*cos(teta2)).*sin(teta1);r1*sin(teta2)];
samps2 = [(R2+r2*cos(teta3)).*cos(teta1);(R2+r2*cos(teta3)).*sin(teta1);r2*sin(teta3)];
K1 = squareform( pdist(samps1') );
eps1 = 1*median(K1(:));
W1 = exp(-K1.^2/eps1^2);
w_inv1 = diag((1./sum(W1,2)));
P1 = w_inv1*W1;
K2 = squareform( pdist(samps2') );
eps2 = median(K2(:));
W2 = exp(-K2.^2/eps2^2);
w_inv2 = diag((1./sum(W2,2)));
P2 = w_inv2*W2;
P = P2*P1; %AD kernel
[~,~,V] = svd(P);
for ii = 1 : 9
    subplot(3,3,ii); scatter(teta1, V(:,ii+1));
end