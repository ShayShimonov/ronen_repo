%% sec 1 
import Utils.* DM_AD.* Basics.*
% create a 2D uniform sampeled circle and preform basic DM
R = 3;
N = 1000;
teta = rand(1,N)*2*pi;
circ2d = [R*sin(teta);R*cos(teta)];
figure(1)
dm_circ2d = Basic_DM( circ2d,2);
scatter(circ2d(1,:),circ2d(2,:),50,teta,'Fill');axis equal;
figure(2)
scatter(dm_circ2d(1,:),dm_circ2d(2,:),50,teta,'Fill');axis equal;
%% sec 2
% project 2D uniform sampeled circle into 3D and preform basic DM
circ3d =rotz(56)* rotx(30)*roty(12)*[dm_circ2d;ones(1,size(dm_circ2d,2))];
figure(3)
scatter3(circ2d(1,:),circ2d(2,:),circ3d(2,:),50,teta,'Fill');
dm_circ3d = Basic_DM( circ3d,2);
figure(4)
scatter(dm_circ3d(1,:),dm_circ3d(2,:),50,teta,'Fill');axis equal;
%% sec 3
% create 2 toruses with common angle and operate AD
N = 1000;
teta1 = rand(1,N)*2*pi;
teta2 = rand(1,N)*2*pi;
teta3 = rand(1,N)*2*pi;
R1 = 10;
R2 = 20;
r1 = 2;
r2 = 5;
torus1 = [(R1+r1*cos(teta2)).*cos(teta1);(R1+r1*cos(teta2)).*sin(teta1);r1*sin(teta2)];
torus2 = [(R2+r2*cos(teta3)).*cos(teta1);(R2+r2*cos(teta3)).*sin(teta1);r2*sin(teta3)];
figure(5);
scatter3(torus2(1,:),torus2(2,:),torus2(3,:), 100, teta1, 'Fill'); axis equal;
figure(6);
scatter3(torus1(1,:),torus1(2,:),torus1(3,:), 100, teta1, 'Fill'); axis equal;
common  = Basic_AD( torus1 , torus2,2,1 );
figure(7);
scatter(common(1,:),common(2,:),50, teta1, 'Fill'); axis equal;
common2  = Basic_AD_mod( torus1 , torus2,2,1 );
figure(8);
scatter(common2(1,:),common2(2,:),50, teta1, 'Fill'); axis equal;
