function [] = visulizeKinshipResults(ad_scores,refined_ad_scores,labels, more_title)
%UNTITLED Summary of this function goes here
if nargin ==3
    more_title = "";
end
ad_postives = ad_scores(labels==1);
ad_negitives = ad_scores(labels==0);
[min_ad_negitives,max_ad_negitives,mean_ad_negitives,std_ad_negitives] = Utils.stat(ad_negitives);
[min_ad_postives,max_ad_postives,mean_ad_postives,std_ad_postives] = Utils.stat(ad_postives);

r_ad_postives = refined_ad_scores(labels==1);
r_ad_negitives = refined_ad_scores(labels==0);
[min_r_ad_negitives,max_r_ad_negitives,mean_r_ad_negitives,std_r_ad_negitives] = Utils.stat(r_ad_negitives);
[min_r_ad_postives,max_r_ad_postives,mean_r_ad_postives,std_r_ad_postives] = Utils.stat(r_ad_postives);


figure();
scatter(1:numel(ad_postives),ad_postives,'r');
hold on;
plot(1:numel(ad_postives), mean_ad_postives*ones(1,numel(ad_postives)),'r');
scatter(1:numel(ad_negitives),ad_negitives,'b');
plot(1:numel(ad_negitives), mean_ad_negitives*ones(1,numel(ad_negitives)),'b');
hold off;
legend('posotive','negitive');
ad_tot_score= sum(ad_postives)-sum(ad_negitives);
title(['naive ad: score is ', num2str(ad_tot_score), more_title]);

figure();
scatter(1:numel(r_ad_postives),r_ad_postives,'r');
hold on;
plot(1:numel(r_ad_postives), mean_r_ad_postives*ones(1,numel(r_ad_postives)),'r');
scatter(1:numel(r_ad_negitives),r_ad_negitives,'b');
plot(1:numel(r_ad_negitives), mean_r_ad_negitives*ones(1,numel(r_ad_negitives)),'b');
hold off;
legend('posotive','negitive');
refined_ad_tot_score = sum(r_ad_postives)-sum(r_ad_negitives);
title(['refined ad: score is ', num2str(refined_ad_tot_score), more_title]);
end

