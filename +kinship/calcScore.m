function [score] = calcScore(C,eig_vector,score_type,L)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

switch score_type
    case 'normalized'
        norm_eig = Utils.normTo01(eig_vector);
        score_mat = C'*diag(norm_eig)*C;
        a = diag(score_mat);
        b = diag(C'*C);
        score = norm(a(1:L))/norm(b(1:L));
    case 'eigen_values'
        score = sum(eig_vector(1:L));
    case 'regular'
        score_mat = C'*diag(eig_vector)*C;
        a = diag(score_mat);
        b = diag(C'*C);
        score = norm(a(1:L))/norm(b(1:L));
    case 'all'
        norm_eig = Utils.normTo01(eig_vector);
        score_mat = C'*diag(norm_eig)*C;
        a = diag(score_mat);
        b = diag(C'*C);
        score_n = norm(a(1:L))/norm(b(1:L));
        score_ee = sum(eig_vector(1:L));
        score_mat_r = C'*diag(eig_vector)*C;
        a_r = diag(score_mat_r);
        score_r = norm(a_r(1:L))/norm(b(1:L)); 
        score = [score_n, score_ee, score_r];
end

end

