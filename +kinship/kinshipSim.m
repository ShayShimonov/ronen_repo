function [] = kinshipSim(DATA_FOLDER_PATH,LABEL_PATH,PATCH_SIZE,STRIDE_SIZE,KERNEL_TYPE,L)
    try
        labels_mat = load(LABEL_PATH);
        [pairs_num,~] = size(labels_mat.pairs);
        labels = cell2mat(labels_mat.pairs(:,2));
        labels = labels(1:pairs_num);

        ad_scores = zeros(pairs_num,L,3);
        refined_ad_scores = zeros(pairs_num,L,3);
        for k = 1:pairs_num
          couple1_fn = labels_mat.pairs(k,3); %sprintf('fs_00%d_1.jpg', k);
          couple2_fn = labels_mat.pairs(k,4); %sprintf('fs_00%d_2.jpg', k);
          couple1_p = fullfile(DATA_FOLDER_PATH, string(couple1_fn));
          couple2_p = fullfile(DATA_FOLDER_PATH, string(couple2_fn));
          if (exist(couple1_p, 'file') && exist(couple2_p, 'file') )
            couple1_img = Utils.normTo01(im2double(imread(couple1_p)));
            couple2_img = Utils.normTo01(im2double(imread(couple2_p)));
          else
            warningMessage = sprintf('Warning: image file does not exist:\n%s', couple1_p);
        %     uiwait(warndlg(warningMessage));
            continue;
          end
          for l = 1:L
              try
                  refined_ad_scores(k,l,:) = kinship.processCoupleAD(couple1_img,couple2_img,PATCH_SIZE,STRIDE_SIZE, KERNEL_TYPE, 'all',1);
                  ad_scores(k,l,:) = kinship.processCoupleAD(couple1_img,couple2_img,PATCH_SIZE,STRIDE_SIZE, KERNEL_TYPE, 'all',0);

              catch ME
                  warning('something did not work assigning a value of 0');
                  refined_ad_scores(k,l,:) = 0;
                  ad_scores(k,l,:) = 0;
                  disp(ME.message);
              end    
          end
        end
        refined_ad_tot_score = zeros(L,3);
        ad_tot_score = zeros(L,3);
        refined_ad_sep_score = zeros(L,3);
        ad_sep_score = zeros(L,3);
        for l = 1:L
            for i = 1:3
                r_ad_sc = refined_ad_scores(:,l,i);
                ad_sc = ad_scores(:,l,i);
                refined_ad_tot_score(l,i) = labels'*(r_ad_sc)-(1-labels)'*(r_ad_sc);
                ad_tot_score(l,i) = labels'*(ad_sc)-(1-labels)'*(ad_sc);
                refined_ad_sep_score(l,i) = mean(find(r_ad_sc.*labels))- mean(find(r_ad_sc.*(1-labels)));
                ad_sep_score(l,i) =  mean(find(ad_sc.*labels))- mean(find(ad_sc.*(1-labels)));
            end
        end

        %% save results
        current_folder = pwd;
        [~,fol,~] = fileparts(DATA_FOLDER_PATH);
        results_path = fullfile(current_folder, '+kinship\results',fol);
        if(~exist(results_path,'dir'))
            mkdir(results_path);
        end
        results_file = fullfile(results_path,[KERNEL_TYPE,'_',num2str(PATCH_SIZE(1)),'_',num2str(STRIDE_SIZE(1)),'.mat']);
        try
            save(results_file, 'ad_scores','refined_ad_scores','L','DATA_FOLDER_PATH','LABEL_PATH','PATCH_SIZE,STRIDE_SIZE','refined_ad_tot_score','ad_tot_score','refined_ad_sep_score','ad_sep_score');
        catch ME
             save(results_file);
             disp(['Error:', ME.message]);
        end
    catch ME
        disp(['Error:', ME.message]);
        save( fullfile(current_folder, '+kinship\results','code_with_errors.mat'));
    end
end
