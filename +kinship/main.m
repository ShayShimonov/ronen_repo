
% try
%     L= 10;
%     cur_fol = pwd;
%     DATA_FOLDER_PATH = fullfile(cur_fol,'+kinship\KinFaceW1\images\father-son');
%     LABEL_PATH = fullfile(cur_fol,'+kinship\KinFaceW1\meta_data\fs_pairs.mat');
%     PATCH_SIZE = [4,4];
%     STRIDE_SIZE = [2,2];
%     KERNEL_TYPE = 'regular';
%     kinship.kinshipSim(DATA_FOLDER_PATH,LABEL_PATH,PATCH_SIZE,STRIDE_SIZE,KERNEL_TYPE,L);
% catch
%     dbstop 
% end

for i= 1:10
    for k = 1:3
        kinship.visulizeKinshipResults(ad_scores(:,i,k),refined_ad_scores(:,i,k),labels,(['L =',num2str(i),' meth = ', num2str(k)]));
    end
end