function [score] = processCoupleAD(im1,im2,patch_size,stride_size, kernel_type,refined)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
import Utils.* DM_AD.* OximeterEx.*
samps1 = kinship.im2patchs(im1, patch_size, stride_size);
samps2 = kinship.im2patchs(im2, patch_size, stride_size);
if refined
    [kernel_AD] = DM_AD.Refined_AD_ker(samps1,samps2,kernel_type);
else
    [kernel_AD] = DM_AD.Basic_AD_knn( samps1 , samps2, 1,[0,0],0,kernel_type);
end
    score = norm(kernel_AD,'fro');
end

