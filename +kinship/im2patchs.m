function [patches,img] = im2patchs(img , patch_size, stride_size)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
m = size(img,1);
n = size(img,2);
p_in_m = floor((m-patch_size(1))/stride_size(1));
p_in_n = floor((n-patch_size(2))/stride_size(2));
a = mod([m-patch_size(1) n-patch_size(2)], stride_size);
if(a ~=0) 
      warning('img cant be divided fully in to patches in given size, thus will be trimmed')
      img = img(1:(m-a(1)),1:(n-a(2)));
end

patches = zeros(patch_size(1)*patch_size(2)*size(img,3),p_in_m*p_in_n);
for i = 1:stride_size(1): (m-patch_size(1))
    for k = 1:stride_size(2): (n-patch_size(2))
        curr_patch = img((i):(i+patch_size(1)-1),(k):(k+patch_size(2)-1),:);
        i_ind = (i-1)/stride_size(1)+1;
        k_ind = (k-1)/stride_size(2)+1;
        patches(:,(i_ind-1)*k_ind+k_ind) = curr_patch(:);
    end
end

end

