function [score_img_n,score_img_refined_n] = scoreImages(img,t_1,patch_size, stride_size,template_num)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
[tm,tn,~] = size(t_1);
tm = tm -(1 -mod(tm,2));
tn = tn - (1 -mod(tn,2));
t_1 = t_1(1:tm,1:tn,:);
[score_img, score_img_refined, patch_size] = Pattern_match.templateMatch(img,t_1,patch_size,stride_size);
%%
score_img_n = Utils.normTo01(score_img);
score_img_refined_n = Utils.normTo01(score_img_refined);
[v_sc, i_sc] = max(score_img_n(:));
[m_v,n_v] = ind2sub(size(score_img),i_sc);
[v_sc_r, i_sc_r] = max(score_img_refined_n(:));
[m_v_r,n_v_r] = ind2sub(size(score_img_refined),i_sc_r);
save(strcat("+Pattern_match\catalunya\scoreFor",num2str(template_num),"_", num2str(patch_size(1)), "_", num2str(patch_size(2)), "_", num2str(stride_size(1)), "_", num2str(stride_size(2))));
end

