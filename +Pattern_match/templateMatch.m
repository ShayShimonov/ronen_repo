function [score_img, score_img_refined, patch_size] = templateMatch(img,tamplate,patch_size,stride_size)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
KERNEL_TYPE = 'symmetric';
[tm,tn,~] = size(tamplate);
tm = tm -(1 -mod(tm,2));
tn = tn - (1 -mod(tn,2));
tamplate = tamplate(1:tm,1:tn,:);
a = mod([tm-patch_size(1) tn-patch_size(2)], stride_size);
if(a ~=0) 
      warning(['tamplate was trimmed to fit parameters of the problem to size. new size is ',num2str(size(tamplate))]);
      patch_size = patch_size +a;
end
marg_tm = (tm-1)/2;
marg_tn = (tn-1)/2;
img_padded = padarray(img,[marg_tm marg_tn 0], 'circular');
[pm,pn,~] = size(img_padded);
score_img = zeros(size(img));
score_img_refined = zeros(size(img));
for i = (1+marg_tm) :(pm-marg_tm)
    for j = (1+marg_tn):(pn-marg_tn)
       curr_patch = img_padded((i-marg_tm):(i+marg_tm), (j-marg_tn):(j+marg_tn),:);
       score_img(i-marg_tm, j-marg_tn) = kinship.processCoupleAD(curr_patch,tamplate,patch_size,stride_size, KERNEL_TYPE, 0);
       score_img_refined(i-marg_tm, j-marg_tn) = kinship.processCoupleAD(curr_patch,tamplate,patch_size,stride_size, KERNEL_TYPE, 1); 
    end
    disp([num2str((i-marg_tm)/pm*100), ' %']);
end
end

