close all;
clear all;
%% 
img = im2double(imread(fullfile(pwd,"+Pattern_match\catalunya\catalunya.jpg")));
patch_sizes = [80 80; 80 80; 60 60; 60 60; 40 40];
stride_size = [50 50; 30 30; 40 40; 20 20; 20 20];
%%
score_img_n = cell(5,3);
for ii=1:3
    for k = 1:size(patch_sizes,1)
        try
            t_1 = im2double(imread(fullfile(pwd,strcat("+Pattern_match\catalunya\Template",num2str(ii),".jpg"))));
            [score_img_n{k,ii}.score_img_n,score_img_n{k,ii}.score_img_refined_n] = Pattern_match.scoreImages(img,t_1,patch_sizes(k,:), stride_size(k,:),k);
        catch ME
            disp(ME.message);
            continue
        end
    end
end


%% corr results
corr_score_map = zeros(size(img,1),size(img,2),3);
for ii=1:3
    t_1 = im2double(imread(fullfile(pwd,strcat("+Pattern_match\catalunya\Template",num2str(ii),".jpg"))));
    t_1 = t_1 + 0.04*randn(size(t_1));
    [corr_score_map(:,:,ii), marg_m,marg_n] = Pattern_match.corMatching(img,t_1);
    c = corr_score_map(:,:,ii);
    Pattern_match.heatMapShow(img,c);
    title(strcat("corr map view for template ", num2str(ii))); 
    figure();
    suptitle(strcat("corr result template ", num2str(ii))); 
    subplot(1,2,1);
    imshow(img);
    [ypeak, xpeak] = find(c==max(c(:)));
    drawrectangle(gca,'Position', [xpeak-marg_m+1, ypeak-marg_n+1, size(t_1,2), size(t_1,1)]);
    subplot(1,2,2);
    imshow(t_1);
end
%% our results
for ii=1:3
    for k = 1:size(patch_sizes,1)
        t_1 = im2double(imread(fullfile(pwd,strcat("+Pattern_match\catalunya\Template",num2str(ii),".jpg"))));
        [tm,tn,~] = size(t_1);
        tm = tm -(1 -mod(tm,2));
        tn = tn - (1 -mod(tn,2));
        marg_tm = (tm-1)/2;
        marg_tn = (tn-1)/2;
        try
            c_n = score_img_n{k,ii}.score_img_n;
            Pattern_match.heatMapShow(img,c_n);
            title(strcat("naive AD map view for template ", num2str(ii),"p",num2str(patch_sizes(k,:)),"s",num2str(stride_size(k,:)))); 
            figure();
            suptitle(strcat("naive AD result template ", num2str(ii),"p",num2str(patch_sizes(k,:)),"s",num2str(stride_size(k,:)))); 
            subplot(1,2,1);
            imshow(img);
            [ypeak_n, xpeak_n] = find(c_n==max(c_n(:)));
            drawrectangle(gca,'Position', [xpeak_n-marg_m+1, ypeak_n-marg_n+1, size(t_1,2), size(t_1,1)]);
            subplot(1,2,2);
            imshow(t_1);
            c_r = score_img_n{k,ii}.score_img_refined_n;
            Pattern_match.heatMapShow(img,c_r);
            title(strcat("refined AD map view for template ", num2str(ii),"p",num2str(patch_sizes(k,:)),"s",num2str(stride_size(k,:)))); 
            figure();
            suptitle(strcat("refined AD result template ", num2str(ii),"p",num2str(patch_sizes(k,:)),"s",num2str(stride_size(k,:)))); 
            subplot(1,2,1);
            imshow(img);
            [ypeak_r, xpeak_r] = find(c_r==max(c_r(:)));
            drawrectangle(gca,'Position', [xpeak_r-marg_m+1, ypeak_r-marg_n+1, size(t_1,2), size(t_1,1)]);
            subplot(1,2,2);
            imshow(t_1);
        catch ME
            disp(ME.message);
            continue
        end
    end
end
%% show loaded results
% t_1 = im2double(imread(fullfile(pwd,strcat("+Pattern_match\catalunya\Template",num2str(template_num),".jpg"))));
% [tm,tn,~] = size(t_1);
% tm = tm -(1 -mod(tm,2));
% tn = tn - (1 -mod(tn,2));
% marg_tm = (tm-1)/2;
% marg_tn = (tn-1)/2;
% c_n = score_img_n;
% Pattern_match.heatMapShow(img,c_n);
% title(strcat("naive AD map view for template ", num2str(template_num),"p",num2str(patch_size),"s",num2str(stride_size))); 
% figure();
% suptitle(strcat("naive AD result template ", num2str(template_num),"p",num2str(patch_size),"s",num2str(stride_size))); 
% subplot(1,2,1);
% imshow(img);
% [ypeak_n, xpeak_n] = find(c_n==max(c_n(:)));
% drawrectangle(gca,'Position', [xpeak_n-marg_tm+1, ypeak_n-marg_tn+1, size(t_1,2), size(t_1,1)]);
% subplot(1,2,2);
% imshow(t_1);
% c_r = score_img_refined_n;
% Pattern_match.heatMapShow(img,c_r);
% title(strcat("refined AD map view for template ", num2str(template_num),"p",num2str(patch_size),"s",num2str(stride_size))); 
% figure();
% suptitle(strcat("refined AD result template ", num2str(template_num),"p",num2str(patch_size),"s",num2str(stride_size))); 
% subplot(1,2,1);
% imshow(img);
% [ypeak_r, xpeak_r] = find(c_r==max(c_r(:)));
% drawrectangle(gca,'Position', [xpeak_r-marg_tm+1, ypeak_r-marg_tn+1, size(t_1,2), size(t_1,1)]);
% subplot(1,2,2);
% imshow(t_1);
% 
