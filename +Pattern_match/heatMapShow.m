function [] = heatMapShow(img1,map)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
K=5;
figure;
% hm = heatmap(map);
h = imshow(img1);
h.AlphaData = ones([size(img1,1),size(img1,2)])*0.5;
hold on;
l = imshow(cat(3,map,map,zeros(size(map))));
% colorbar;
l.AlphaData = ones(size(map))*0.5;
[~,inds] = maxk(map(:),K);
[ind_x,ind_y] = ind2sub(size(map),inds);
scatter(ind_y,ind_x,'rO');
end

