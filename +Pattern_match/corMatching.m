function [score_map,marg_tm,marg_tn] = corMatching(img,template)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
[tm,tn,~] = size(template);
tm = tm -(1 -mod(tm,2));
tn = tn - (1 -mod(tn,2));
template = template(1:tm,1:tn,:);
marg_tm = (tm-1)/2;
marg_tn = (tn-1)/2;
% img_padded = padarray(img,[marg_tm marg_tn 0], 'circular');
[im,in,~] = size(img);
map = normxcorr2(template,img);
score_map = map((1+marg_tm):(im+marg_tm),(1+marg_tn):(in+marg_tn));
end

