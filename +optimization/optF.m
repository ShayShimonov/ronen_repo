function [ del2 ] = optF( c,n,dims,x)
%the function that we want to minimze under constrains
%c - processes handles
%n- number of points in disterbution
%dims - dimentions of each hidden variable
%x - epsilons of AD : target variable
del2 = (c(2)*n^2*x(2)^(dims(1)+dims(3))*x(1)^(dims(1)+dims(2)))^(-1/(dims(2)+1));

end

