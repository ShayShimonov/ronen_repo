import Utils.* optimization.* 
% initilize according to problem
dims = [1 1 1];
c = [1/12 4/12];
n = 5000;
% define all functions needed to optimization problem
fun = @(x) optF( c,n,dims,x);
%del1 = 