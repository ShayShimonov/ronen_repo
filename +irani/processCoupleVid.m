function [score] = processCoupleVid(vid1,vid2,kernel_type,refined)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
import Utils.* DM_AD.* OximeterEx.*
[m,n,d,t] = size(vid1);
samps1 = zeros(m*n*d,t);
samps2 = zeros(m*n*d,t);
for ii = 1:t
    temp1 = vid1(:,:,:,ii);
    temp2 = vid2(:,:,:,ii);
    samps1(:,ii) = temp1(:);
    samps2(:,ii) = temp2(:);
end
if refined
    [kernel_AD] = DM_AD.Refined_AD_ker(samps1,samps2,kernel_type);
else
    [kernel_AD] = DM_AD.Basic_AD_spec_ker( samps1 , samps2, 1,[0,0],0,kernel_type);
end
    score = norm(kernel_AD,'fro');
end



