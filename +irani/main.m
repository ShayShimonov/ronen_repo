%%
clear all; 
close all;
%% get data
KERNEL_TYPE = 'symmetric';
vid = VideoReader(fullfile(pwd,'+irani\ballet\Birmingham_Ballet_data.avi'));
t = VideoReader(fullfile(pwd,'+irani\ballet\Birmingham_ballet_turn_pa_template.avi'));
t_data = read(t);
[mt,nt,ct,tt] = size(t_data);
mt = mt -(1 -mod(mt,2));
nt = nt - (1 -mod(nt,2));
t_data = t_data(1:mt,1:nt,:,:);
marg_mt = (mt-1)/2;
marg_nt = (nt-1)/2;
%%
curr_patch_data = zeros(vid.Height,vid.Width,ct,tt);
for ii = 1:tt
    curr_patch_data(:,:,:,tt) = readFrame(vid);
end
score_img = NaN;
score_img_refined = NaN;
numframe = 1;
while vid.hasFrame()
    score_img_curr = zeros(vid.Height,vid.Width);
    score_img_refined_curr = zeros(vid.Height,vid.Width);
    for i = (1+marg_mt) :(vid.Height + marg_mt)
        for j = (1+marg_nt):(vid.Width +marg_nt)
           vid_padded = padarray(curr_patch_data,[marg_mt marg_nt 0 0]);
           curr_patch = vid_padded((i-marg_mt):(i+marg_mt), (j-marg_nt):(j+marg_nt),:,:);
           score_img_curr(i-marg_mt, j-marg_nt) = irani.processCoupleVid(curr_patch,t_data, KERNEL_TYPE, 0);
           score_img_refined_curr(i-marg_mt, j-marg_nt) = irani.processCoupleVid(curr_patch,t_data, KERNEL_TYPE, 1); 
        end
        disp([num2str((i-marg_mt)/vid.Width*100), ' %']);
    end
    if numframe == 1
        score_img = score_img_curr;
        score_img_refined = score_img_refined_curr;
    else
        score_img = cat(3,score_img,score_img_curr);
        score_img_refined = cat(3,score_img_refined,score_img_refined_curr);
    end
    curr_patch_data = circshift(curr_patch_data,-1,4);
    curr_patch_data(:,:,:,tt) = readFrame(vid);
    numframe = numframe +1;                                                                                     
end
save('ballet.mat');


