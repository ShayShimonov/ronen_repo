function [ mseEpsEr,mseF] = CheckEpsSim_present(epsSimNum,ReverseOrder )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% ReverseOrder - if true show Reversed order sim
% important note - the reconstraction of x using DM with 0.1*median as
% epsilon which we are trying to achive, using also deltaY deltaZ from sims
% using 0.1*median is arbitrary according to expirience.
SIM_PATH = 'simulationFiles\optEps\knn\';
if nargin ==1
    ReverseOrder =0;
end
import Utils.* DM_AD.* OptEpsSim.* ADsim.*
if(ReverseOrder)
    load([SIM_PATH,'RepsSimKnn',num2str(epsSimNum)]);
else
    load([SIM_PATH,'epsSimKnn',num2str(epsSimNum)]);
end
% load(['+OptEpsSim\epsSimKnn',num2str(epsSimNum)]);
n = size(xy,2);
% c= sqrt([(1/12)*bounds(3)^2,(1/12)*bounds(2)^2]);
c= [std(xy(:)), std(xz(:))];
[AlgE1,AlgE2] =  Utils.EpsAlg(n,c,dims);
% [AlgE1,AlgE2] =  Utils.NewEpsAlg(n,c,dims);
K1 = squareform( pdist(xy') );
epsMed1 = median(K1(:));
K2 = squareform( pdist(xz') );
epsMed2 = median(K2(:));
[ mseEpsEr,mseF] = CreateFPlot(f, epss, AlgE1, AlgE2, epsMed1, epsMed2, {"Reconstruction error as a func of \epsilon_1, \epsilon_2", strcat("d_x=", num2str(dims(1)), " d_y=", num2str(dims(2)), " d_z=", num2str(dims(3)))});
CreateFPlot(g, epss, AlgE1, AlgE2, epsMed1, epsMed2, "Eigen Values vector error as a func of \epsilon_1, \epsilon_2"); 
if(ReverseOrder)
    CreateFPlot(fR, epss, AlgE1, AlgE2, epsMed1, epsMed2, "f R");
end

end

function [ mseEpsEr,mseF] = CreateFPlot(func, epss, AlgE1, AlgE2, epsMed1, epsMed2, func_name)
    [fMin,ind] = min(func(:));
    [X,Y] = meshgrid(epss);
    fRefined =  interp2(X,Y,func,AlgE1,AlgE2);
    fNaive = interp2(X,Y,func,epsMed1,epsMed2);
    mseF = norm(fRefined-fMin);
    [eps2Ind, eps1Ind] = ind2sub(size(func),ind);
    mseEpsEr = norm([(epss(eps1Ind)-AlgE1), (epss(eps2Ind)-AlgE2)]);
    epssLog =(epss);
    figure;
    [X,Y] = meshgrid(epssLog);
    mesh(X,Y,func);%set(gca, 'XScale', 'log', 'YScale', 'log');
    % imagesc(func);
    colorbar;
    hold on;
    plot3( epssLog(eps1Ind), epssLog(eps2Ind),(fMin),'r+', 'MarkerSize',30);
    plot3((AlgE1),(AlgE2),fRefined,'m*', 'MarkerSize', 30);
    plot3((epsMed1),(epsMed2),fNaive,'k*', 'MarkerSize', 30);
    hold off;
    set(gca, 'XScale', 'log', 'YScale', 'log');
    xlabel('log epsX');
    ylabel('log epsY');
    l = legend('f','min value','refined AD guess', 'naive median guess');
    l.FontSize = 16;
%     if(~ReverseOrder)
%         title(['bounds: ',num2str(bounds)]);
%     else
%         title(['bounds: ',num2str(bounds),' Reverse Order Kernel']);
%     end
    title(func_name);
end

