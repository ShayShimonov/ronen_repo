% epsSimNum = 5;
% EpsPoints = 120;
% sameEps = 0;
% ReverseOrder = 1;
% bounds = [1,1,1];
% dims = [3,4,3];
% OptEpsSim.epsilonSim( epsSimNum,EpsPoints,sameEps,ReverseOrder,bounds,dims );
% %%
% epsSimNum = 6;
% EpsPoints = 120;
% sameEps = 0;
% ReverseOrder = 1;
% bounds = [1,2,1];
% dims = [1,1,1];
% OptEpsSim.epsilonSim( epsSimNum,EpsPoints,sameEps,ReverseOrder,bounds,dims );
% epsSimNum = 8;
% EpsPoints = 150;
% sameEps = 0;
% ReverseOrder = 1;
% bounds = [1,1,1];
% dims = [23,34,45];
% OptEpsSim.epsilonSim( epsSimNum,EpsPoints,sameEps,1,bounds,dims );


% displays epsilon Sim resault as shown in chapter 2.4 
for id =[6,8]
    epsSimNum =id; %number of sim matfile
    ReverseOrder =0; %reverse kernel order, now false 
    OptEpsSim.CheckEpsSim_present(epsSimNum,ReverseOrder);
end