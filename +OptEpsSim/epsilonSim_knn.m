function [f,xGT,xy,xz,epss] = epsilonSim_knn( epsSimNum,EpsPoints,sameEps,ReverseOrder,bounds,dims )
% epsSimNum - number of Sim to save
% bounds - row vector which specifies disturpution upper bound assuming lower bound is zero
% dims - dimentions of hidden variable distrebution
% EpsPoints - number of points in 1 axe in func
% ReverseOrder - if true reverse order of AD kernel is taken
% Note: This function uses knn Kernel as oppose to regular kernels. 
import Utils.* DM_AD.* OptEpsSim.*
SIM_PATH = 'simulationFiles\optEps\knn\';
%% create xyz uniform disterbution
if nargin==3
    bounds = [1 1 1];
    dims = [1 1 1];
    ReverseOrder = 0;
end
[xy,xz] = UniformDist(1000,bounds,dims);
dimX = dims(1);
%% run basic DM on x - phiX
[xGT] = Basic_DM_knn( xy(1:dims(1),:),dims(1));
%% create 3-D function of AD reconstruction error compare to Phix
if(sameEps)
    f = zeros(1,EpsPoints);
    if(ReverseOrder)
        fR = zeros(1,EpsPoints);
    end
else
    f = zeros(EpsPoints,EpsPoints);
    if(ReverseOrder)
        fR = zeros(EpsPoints,EpsPoints);
    end
end
epss = (logspace(-3,0,EpsPoints)/2);
for kk=1:(1+(EpsPoints-1)*(1-sameEps))
    for ii=1:EpsPoints
        if(sameEps)
            [ common ] = Basic_AD_knn( xy , xz, dimX,-1,[epss(ii),epss(ii)]);
            f(ii) = norm(common-xGT);
            if(ReverseOrder)
                [ commonR ] = Basic_AD_knn( xy , xz, dimX,-1,[epss(ii),epss(ii)],ReverseOrder);
                fR(ii) = norm(commonR-xGT);
            end
        else
            [ common ] = Basic_AD_knn( xy , xz, dimX,-1,[epss(kk),epss(ii)]);
            f(kk,ii) = norm(common-xGT);
            if(ReverseOrder)
                [ commonR ] = Basic_AD_knn( xy , xz, dimX,-1,[epss(kk),epss(ii)],ReverseOrder);
                fR(kk,ii) = norm(commonR-xGT);
            end
        end
    end
end
save([SIM_PATH,'epsSim',num2str(epsSimNum)],'f','xGT','xy','xz','dims','bounds','epss');
if(ReverseOrder)
    save([SIM_PATH,'RepsSim',num2str(epsSimNum)],'fR','xGT','xy','xz','dims','bounds','epss');
end
end