function [ mseEpsEr,mseF] = CheckEpsSim(epsSimNum,ReverseOrder )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% ReverseOrder - if true show Reversed order sim
% important note - the reconstraction of x using DM with 0.1*median as
% epsilon which we are trying to achive, using also deltaY deltaZ from sims
% using 0.1*median is arbitrary according to expirience. 
SIM_PATH = 'simulationFiles\optEps\knn';
bounds=0;
sameEps = 0; %default
if nargin ==1
    ReverseOrder =0;
end
import Utils.* DM_AD.* OptEpsSim.* ADsim.*
if(ReverseOrder)
    load([SIM_PATH,'\RepsSim',num2str(epsSimNum)]);
    f =fR;
else
    load([SIM_PATH,'\epsSim',num2str(epsSimNum)]);
end
n = size(xy,2);
[deltaZ,~,deltaY,~,~,~ ] = sims( 1,0,0,bounds,epsSimNum );
epsGuessNoLog1 = ((1/(bounds(3)*n))*deltaZ^(-dims(3)))^(1/(dims(1)+dims(2)));% original  guess
epsGuessNoLog2 = (((1/(bounds(2)*n^2))*deltaY^(-dims(2)))^(1/(dims(1)+dims(3))))/epsGuessNoLog1;
epsGuessWLog1ap1 = ((1/(bounds(3)*n))*deltaZ^(-dims(3))*(1/deltaZ-1))^(1/(dims(1)+dims(2)));% with out omiting log expression
% epsGuessWLog1ap2 = (((1/n^2)*deltaY^(-dims(2))*(1/deltaY-1))^(1/(dims(1)+dims(2))))/epsGuessNoLog1;% replacing with taylor around 1
epsGuessWLog1ap2 = (((bounds(3)/bounds(2))*(1/n)*(deltaZ^(dims(3)+1))/(deltaY^(dims(2)+1))*(1-deltaY)/(1-deltaZ))^(1/(dims(1)+dims(3))));% replacing log(x) with x-1
c= sqrt([(1/12)*bounds(3)^2,(1/12)*bounds(2)^2]);
% c=[bounds(3),bounds(2)];
[AlgE1,AlgE2] =  Utils.EpsAlg(n,c,dims);
% epsShay1 = ((1/n)*deltaZ^(-dims(3))*(1/deltaZ-1)*1/12);
% epsShay2 = 2*((1/n^2)*deltaY^(-dims(2))*(1/deltaY-1)*(1/12)^2)/epsShay1;
figure();
[fMin,ind] = min(f(:));
[~,epsGuess1ClosestInd] = min(abs(epss-epsGuessNoLog1));
[~,epsGuess2ClosestInd] = min(abs(epss-epsGuessNoLog2));
[~,epsGuessWLog1ap1ClosestInd] = min(abs(epss-epsGuessWLog1ap1 ));
[~,epsGuessWLog1ap2ClosestInd] = min(abs(epss-epsGuessWLog1ap2));
% [~,epsShay1Ind] = min(abs(epss-epsShay1));
% [~,epsShay2Ind] = min(abs(epss-epsShay2));
[~,AlgE1Ind] = min(abs(epss-AlgE1));
[~,AlgE2Ind] = min(abs(epss-AlgE2));
[~,epsTRyInd] = min(abs(epss-1/12)); %try var f = epss
if(sameEps)
    fEps1 = mean(f(epsGuess1ClosestInd));
    fEps2 = mean(f(epsGuess2ClosestInd));
    mseF = [norm(fEps1-fMin),norm(fEps2-fMin)];
    mseEpsEr = [norm(epsGuessNoLog1-epss(ind)),norm(epsGuessNoLog2-epss(ind))];
    semilogx(epss,f);
    hold on;
    semilogx(epss(ind),fMin,'*','MarkerSize',20);
    semilogx(epsGuessNoLog1,mean(f(epsGuess1ClosestInd)),'+','MarkerSize',20);
    semilogx(epsGuessNoLog2,mean(f(epsGuess2ClosestInd)),'+','MarkerSize',20);
    semilogx(epsGuessWLog1ap1,mean(f(epsGuessWLog1ap1ClosestInd)),'+','MarkerSize',20);
    semilogx(epsGuessWLog1ap2,mean(f(epsGuessWLog1ap2ClosestInd)),'+','MarkerSize',20);
    semilogx(1/12,mean(f(epsTRyInd)),'+','MarkerSize',20);
%     semilogx(epsShay1,mean(f(epsShay1Ind)),'+','MarkerSize',20);
%     semilogx(epsShay2,mean(f(epsShay2Ind)),'+','MarkerSize',20);
    hold off;
    xlabel('log eps');
    ylabel('log f');
    legend('plot','min','guess1 NL','guess2 NL','guess1 WL1','guess2 WL1','same');%,'Stry1','Stry2');
else
    fGuess =  mean(f(epsGuess1ClosestInd,epsGuess2ClosestInd));
    mseF = norm(fGuess-fMin);
    [eps1Ind,eps2Ind] = ind2sub(size(f),ind);
    mseEpsEr = norm([(epss(eps1Ind)-epss(epsGuess1ClosestInd)), (epss(eps2Ind)-epss(epsGuess2ClosestInd))]);
    epssLog =(epss);
    [X,Y] = meshgrid(epssLog);
    mesh(X,Y,(f));%set(gca, 'XScale', 'log', 'YScale', 'log');
    hold on;
    plot3( epssLog(eps1Ind), epssLog(eps2Ind),(fMin),'r+', 'MarkerSize',20);
    plot3((epsGuessNoLog1),(epsGuessNoLog2),( mean(f(epsGuess2ClosestInd,epsGuess1ClosestInd))),'b*', 'MarkerSize', 20);
    plot3((epsGuessWLog1ap1),(epsGuessWLog1ap2),( mean(f(epsGuessWLog1ap2ClosestInd,epsGuessWLog1ap1ClosestInd))),'g*', 'MarkerSize', 20);
    plot3((AlgE1),(AlgE2),( mean(f(AlgE2Ind,AlgE1Ind))),'m*', 'MarkerSize', 24);
    plot3((1/12),(1/12),( mean(f(epsTRyInd,epsTRyInd))),'c*', 'MarkerSize', 20);
%     plot3((epsShay1),(epsShay2),( mean(f(epsShay2Ind,epsShay1Ind))),'k*', 'MarkerSize', 20);
    hold off;
    set(gca, 'XScale', 'log', 'YScale', 'log');
    xlabel('log epsX');
    ylabel('log epsY');
    %zlabel('log f');
    legend('plot','min','guess with out log', 'guess with log(x)~x-1','AlgorithemGuess','same');%,'ShayTry');
    if(~ReverseOrder)
       title(['bounds: ',num2str(bounds)]);
    else
       title(['bounds: ',num2str(bounds),' Reverse Order Kernel']); 
    end
end
end

