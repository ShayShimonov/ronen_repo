function [f,xGT,xy,xz,epss] = epsilonSim( epsSimNum,EpsPoints,sameEps,ReverseOrder,bounds,dims )
% epsSimNum - number of Sim to save
% bounds - row vector which specifies disturpution upper bound assuming lower bound is zero
% dims - dimentions of hidden variable distrebution
% EpsPoints - number of points in 1 axe in func
%  ReverseOrder - if true reverse order of AD kernel is taken
import Utils.* DM_AD.* OptEpsSim.*
%% create xyz uniform disterbution
if nargin==3
    bounds = [1 1 1];
    dims = [1 1 1];
    ReverseOrder = 0;
end
[xy,xz] = UniformDist(1000,bounds,dims);
dimX = dims(1);
%% run basic DM on x - phiX
[xGT, sGT] = DM_AD.Basic_DM_knn( xy(1:dims(1),:),dims(1));
%% create 3-D function of AD reconstruction error compare to Phix
if(sameEps)
    f = zeros(1,EpsPoints);
    g = zeros(1,EpsPoints);
    if(ReverseOrder)
        fR = zeros(1,EpsPoints);
    end
else
    f = zeros(EpsPoints,EpsPoints);
    g = zeros(EpsPoints,EpsPoints);
    if(ReverseOrder)
        fR = zeros(EpsPoints,EpsPoints);
    end
end
epss = (logspace(-3,1,EpsPoints)/2);
for kk=1:(1+(EpsPoints-1)*(1-sameEps))
    for ii=1:EpsPoints
        if(sameEps)
            [ Pcom ] = DM_AD.Basic_AD_knn( xy , xz,-1,[epss(ii),epss(ii)],0,'regular');
            [~,S,V] = svd(Pcom);
            common = (V(:,2:dimX+1))';
            f(ii) = norm(common-xGT);
            g(ii) = norm(diag(S)-diag(sGT));
            if(ReverseOrder)
                [ PcomR ] = DM_AD.Basic_AD_knn( xy , xz,-1,[epss(ii),epss(ii)],ReverseOrder,'regular');
                [~,~,VR] = svd(PcomR);
                commonR = (VR(:,2:dimX+1))';
                fR(ii) = norm(commonR-xGT);
            end
        else
            [ Pcom ] = DM_AD.Basic_AD_knn( xy , xz,-1,[epss(kk),epss(ii)],0,'regular');
            [~,S,V] = svd(Pcom);
            common = (V(:,2:dimX+1))';
            f(kk,ii) = norm(common-xGT);
            g(kk,ii) = norm(diag(S)-diag(sGT));
            if(ReverseOrder)
                [ PcomR ] = DM_AD.Basic_AD_knn( xz , xy,-1,[epss(ii),epss(kk)],ReverseOrder,'regular');
                [~,~,VR] = svd(PcomR);
                commonR = (VR(:,2:dimX+1))';
                fR(kk,ii) = norm(commonR-xGT);
            end
        end
    end
end
save(['+OptEpsSim\epsSimKnn',num2str(epsSimNum)],'g','f','xGT','xy','xz','dims','bounds','epss');
if(ReverseOrder)
    save(['+OptEpsSim\RepsSimKnn',num2str(epsSimNum)],'g','f','fR','xGT','xy','xz','dims','bounds','epss');
end
end