function [] = clustterData(labels,img,tl)
figure;
L = reshape(labels, size(img));
B = labeloverlay(uint8(img),uint8(L));
imshow(B);
title(strcat(tl," overlay"));
figure;
subplot(1,2,1);
imagesc(L)
subplot(1,2,2);
imagesc(img);
sgtitle (strcat(tl," compare"));
