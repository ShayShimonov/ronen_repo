function [] = clustterData2(labels,img,tl, output_dir, name)
L = reshape(labels, size(img));
figure;
subplot(1,2,1);
imagesc(L)
subplot(1,2,2);
imagesc(img);
sgtitle (strcat(tl));
savefig(strcat(output_dir,"\",name,".fig"))