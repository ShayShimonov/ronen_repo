function [output_samp1,output_samp2] = images3d2ad_samps(in_img1,in_img2)
% This function recieves multi_spectral images and turns them to AD samples
% format.
output_samp1 = zeros(size(in_img1,1)*size(in_img1,2), size(in_img1,3));
for ii = 1:size(in_img1,3)
    temp = in_img1(:,:,ii);
    output_samp1(:,ii) = temp(:);
end
output_samp1 = output_samp1';
output_samp2 = zeros(size(in_img2,1)*size(in_img2,2), size(in_img2,3));
for ii = 1:size(in_img2,3)
    temp = in_img2(:,:,ii);
    output_samp2(:,ii) = temp(:);
end
output_samp2 = output_samp2';
end

