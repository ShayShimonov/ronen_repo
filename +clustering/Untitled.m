clear all;
close all;
%% directory
CURRENT_FOLDER = pwd;
OUTPUT_DIR = strcat(CURRENT_FOLDER, "\+clustering\results\new\c_flip\");
%% read data
lidar_path = fullfile(pwd,'+clustering\GRSS2013\2013_DFTC\2013_IEEE_GRSS_DF_Contest_LiDAR.tif');
casi_path = fullfile(pwd,'+clustering\GRSS2013\2013_DFTC\2013_IEEE_GRSS_DF_Contest_CASI.tif');
[A_l,R_l] =  geotiffread(lidar_path);
[A_c,R_c] =  geotiffread(casi_path);
%% create patches results
positions = [50,200];
% patch_sizes = [10 25 35];
[pos_len,~] = size(positions);
for pos_ind = 1:pos_len
    pos = positions(pos_ind,:);
%     clustering.handleByPos(OUTPUT_DIR,A_l,A_c,pos,patch_sizes)
    clustering.handleByPos(OUTPUT_DIR,A_l,A_c,pos)
end