function [] = handleByPos(OUTPUT_DIR,A_l,A_c,pos,patch_sizes)
%HANDLEBYPOS Summary of this function goes here
%   Detailed explanation goes here
if nargin == 4
    USE_PATCHES = false;
else
    USE_PATCHES = true;
end
% %%%%%%%%%%%%%%%%%PARAMETERS%%%%%%%%%%%%%%%%%%%
FRAME_SIZE = [100, 100];
NUMBER_OF_EVAL_TO_USE = 10;
dim = 300;
% %%%%%%%%%%%%%%%%%PARAMETERS%%%%%%%%%%%%%%%%%%%
% crop frame
A_lt = Utils.normelizeByDim(double(A_l(pos(1):(pos(1)+FRAME_SIZE(1)-1),pos(2):(pos(2)+FRAME_SIZE(2)-1))),3);
A_ct = Utils.normelizeByDim(double(A_c(pos(1):(pos(1)+FRAME_SIZE(1)-1),pos(2):(pos(2)+FRAME_SIZE(2)-1),:)),3);
% caluclate samples to AD
[lidar_samp,casi_samp] = clustering.images3d2ad_samps(A_lt, A_ct);
% get AD solutions
[Pcom_reg_ad, eps_ad1, eps_ad2] = DM_AD.Basic_AD_knn( casi_samp , lidar_samp,1,[0 0],0,'regular');
[common_reg_ad, S_reg_ad] = eigs(Pcom_reg_ad,dim);
if ~USE_PATCHES
    [Pcom_reg_rad,rad_eps1_reg , rad_eps2_reg] = DM_AD.Refined_AD_ker( casi_samp , lidar_samp,'regular');
    [common_reg_rad, S_reg_rad] = eigs(Pcom_reg_rad,dim);
    full_title = strcat("pos_",num2str(pos(1)),"_",num2str(pos(2)));
    clustering.plot_result(OUTPUT_DIR, NUMBER_OF_EVAL_TO_USE, common_reg_rad, S_reg_rad, common_reg_ad, S_reg_ad, A_lt, A_ct, full_title)
else
    clustering.handleByPatch(OUTPUT_DIR, patch_sizes,A_ct,A_lt, casi_samp , lidar_samp, NUMBER_OF_EVAL_TO_USE, dim, pos, eps_ad1, eps_ad2, common_reg_ad, S_reg_ad)
end
end

