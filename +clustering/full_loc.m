clear all;
close all;
%% directory
CURRENT_FOLDER = pwd;
OUTPUT_DIR = strcat(CURRENT_FOLDER, "\+clustering\results\full\loc\mat_files\");
%% read data
lidar_path = fullfile(pwd,'+clustering\GRSS2013\2013_DFTC\2013_IEEE_GRSS_DF_Contest_LiDAR.tif');
casi_path = fullfile(pwd,'+clustering\GRSS2013\2013_DFTC\2013_IEEE_GRSS_DF_Contest_CASI.tif');
[A_l,R_l] =  geotiffread(lidar_path);
[A_c,R_c] =  geotiffread(casi_path);
%% positions
positions = [114,1179;19,575;166,1164;238,1383;55,1757;6,810;248,1063;20,1694;50,560];
[pos_len,~] = size(positions);
for pos_ind = 1:pos_len
    pos = positions(pos_ind,:);
    A_lt = double(A_l(pos(1):(pos(1)+99),pos(2):(pos(2)+99)));
    A_ct = double(A_c(pos(1):(pos(1)+99),pos(2):(pos(2)+99),:));
    [LT_x, LT_y] = meshgrid(1:size(A_lt,1),1:size(A_lt,2));
    [CT_x, CT_y] = meshgrid(1:size(A_ct,1),1:size(A_ct,2));
    A_lt = Utils.normelizeByDim(cat(3,A_lt, LT_x, LT_y),3);
    A_ct = Utils.normelizeByDim(cat(3, A_ct, CT_x, CT_y),3);
    % patches
    patch_sizes = [10 25 35];
    % caluclate samples to AD
    [lidar_samp,casi_samp] = clustering.images3d2ad_samps(A_lt, A_ct);
    dim = 300;
    % get refined AD solutions
    [Pcom_reg_ad, eps_ad1, eps_ad2] = DM_AD.Basic_AD_spec_ker( casi_samp , lidar_samp,1,[0 0],0,'regular');
    [common_reg_ad, S_reg_ad] = eigs(Pcom_reg_ad,dim);
    [Pcom_sym_ad] = DM_AD.Basic_AD_spec_ker( casi_samp , lidar_samp,1,[0 0],0,'symmetric');
    [common_sym_ad, S_sym_ad] = eigs(Pcom_sym_ad,dim);
    % get lables
    K = 10;
    reg_ad_labels = clustering.getClustterLabels(K,common_reg_ad);
    sym_ad_labels = clustering.getClustterLabels(K,common_sym_ad);
    for patch_ind =1:3
        patch_size = [patch_sizes(patch_ind) patch_sizes(patch_ind)];
        % epsilon vecs
        [ker_scale_1_reg, ker_scale_2_reg,ker_scale_1_sym,ker_scale_2_sym] = get_eps_vectors(patch_size,A_ct,A_lt);
        % reg eps rad
        rad_eps1_reg = median(ker_scale_1_reg);
        rad_mad1_reg = mad(ker_scale_1_reg,1);
        rad_eps2_reg = median(ker_scale_2_reg);
        rad_mad2_reg = mad(ker_scale_2_reg,1);
        % sym eps rad
        rad_eps1_sym = median(ker_scale_1_sym);
        rad_mad1_sym = mad(ker_scale_1_sym,1);
        rad_eps2_sym = median(ker_scale_2_sym);
        rad_mad2_sym = mad(ker_scale_2_sym,1);
        % get refined AD solutions
        [Pcom_reg_rad] = DM_AD.Basic_AD_knn( casi_samp , lidar_samp,-1,[rad_eps1_reg rad_eps2_reg],0,'regular');
        [common_reg_rad, S_reg_rad] = eigs(Pcom_reg_rad,dim);
        [Pcom_sym_rad] = DM_AD.Basic_AD_knn( casi_samp , lidar_samp,-1,[rad_eps1_sym rad_eps2_sym],0,'symmetric');
        [common_sym_rad, S_sym_rad] = eigs(Pcom_sym_rad,dim);
        % get lables
        reg_rad_labels = clustering.getClustterLabels(K,common_reg_rad);
        sym_rad_labels = clustering.getClustterLabels(K,common_sym_rad);
        % title 
        title = strcat("pos_",num2str(pos(1)),"_",num2str(pos(2)),"patch_size","_",num2str(patch_size(1)),"_",num2str(patch_size(2)));
        % plot results
        create_eps_fig(rad_eps1_reg, rad_mad1_reg, rad_eps2_reg, rad_mad2_reg, eps_ad1, eps_ad2, A_lt(:,:,1), A_ct(:,:,1), reg_ad_labels, reg_rad_labels, title, "Regular Kernel");
        create_eps_fig(rad_eps1_sym, rad_mad1_sym, rad_eps2_sym, rad_mad2_sym, eps_ad1, eps_ad2, A_lt(:,:,1), A_ct(:,:,1), sym_ad_labels, sym_rad_labels, title, "Symmetric Kernel");
        save(strcat(OUTPUT_DIR,title,".mat"));
    end
end














% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% functions %%%%%%%%%%%%%%%%%%%%%%%
function [ker_scale_1_reg, ker_scale_2_reg,ker_scale_1_sym,ker_scale_2_sym] = get_eps_vectors(patch_size,A_ct,A_lt)
    rep_num = 25;
    K =10;
    ker_scale_1_reg = zeros(rep_num,1);
    ker_scale_2_reg = zeros(rep_num,1);
    ker_scale_1_sym = zeros(rep_num,1);
    ker_scale_2_sym = zeros(rep_num,1);
    for rep_ind=1:rep_num
        cur_casi_patch = Utils.rand_patch(A_ct, patch_size);
        cur_lidar_patch = Utils.rand_patch(A_lt, patch_size);
        [cur_lidar_samp,cur_casi_samp] = clustering.images3d2ad_samps(cur_casi_patch, cur_lidar_patch);
        [ ker_scale_1_reg(rep_ind), ker_scale_2_reg(rep_ind)] = DM_AD.Refined_AD_get_eps( cur_casi_samp , cur_lidar_samp);
        [ ker_scale_1_sym(rep_ind), ker_scale_2_sym(rep_ind)] = DM_AD.Refined_AD_get_eps( cur_casi_samp , cur_lidar_samp);
    end
end
 

function [] = create_eps_fig(rad_eps1, rad_mad1, rad_eps2, rad_mad2, eps_ad1, eps_ad2, img_1, img_2, labels_ad, labels_rad, title_in, type)
    L_rad = reshape(labels_rad, size(img_1));
    L_ad = reshape(labels_ad, size(img_1));
    [n_L_rad, n_L_ad] = Utils.syncLabelImages(L_rad,L_ad,10);
    figure;
    subplot(2,2,1);
    imshow(Utils.normTo01(img_1));
    title('first modality');
    subplot(2,2,2);
    imshow(Utils.normTo01(img_2));
    title('second modality');
    subplot(2,2,3);
    n_L_ad = label2rgb(n_L_ad);
    imagesc(n_L_ad);
    pbaspect([1 1 1]);
    title('naive AD');
    subplot(2,2,4);
    n_L_rad = label2rgb(n_L_rad);
    imagesc(n_L_rad);
    pbaspect([1 1 1]);
    title('refined AD');
    eps1_title = strcat("eps rad1 = ",num2str(rad_eps1),"+-",num2str(rad_mad1),"eps naive1 =", num2str(eps_ad1)," dist = ",num2str(abs(eps_ad1-rad_eps1)/rad_mad1)," [mad]");
    eps2_title = strcat("eps rad2 = ",num2str(rad_eps2),"+-",num2str(rad_mad2),"eps naive2 =", num2str(eps_ad2)," dist = ",num2str(abs(eps_ad2-rad_eps2)/rad_mad2)," [mad]");
    sgtitle ({type, strrep(title_in,"_"," "), eps1_title, eps2_title});
    currentFolder = pwd;
    output_directory = strcat(currentFolder, "\+clustering\results\full\loc\figs\");
    savefig(strcat(output_directory,title_in,"_",strrep(type," ","_"),".fig"));
end
