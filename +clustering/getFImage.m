function [f_image] = getFImage(k,V,S)
% This function calculates The geometry of nodal sets and outlier detection
% picture. 
% @inputs:
% k - number of eigenvectors to use
% V - eigen vectors matrix
% S - eigenvalues matrix
ee = diag(S);
ev_used_norm = V(:,2:(k+1))./max(V(:,2:(k+1)));
ev_f = sqrt(ee(2:(k+1)))'.*ev_used_norm;
f_image = sum(ev_f,2);

