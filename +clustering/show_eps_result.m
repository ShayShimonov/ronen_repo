function [] = show_eps_result(ker_scale1_reg, ker_scale2_reg, labels_ad_reg, labels_rad_reg, ker_scale1_sym, ker_scale2_sym, labels_ad_sym, labels_rad_sym, img_1, img_2, samps1, samps2, title_in)
%SHOW_EPS_RESULT Summary of this function goes here
%   Detailed explanation goes here
% reg eps rad
rad_eps1_reg = median(ker_scale1_reg);
rad_mad1_reg = mad(ker_scale1_reg,1);
rad_eps2_reg = median(ker_scale2_reg);
rad_mad2_reg = mad(ker_scale2_reg,1);
% sym eps rad
rad_eps1_sym = median(ker_scale1_sym);
rad_mad1_sym = mad(ker_scale1_sym,1);
rad_eps2_sym = median(ker_scale2_sym);
rad_mad2_sym = mad(ker_scale2_sym,1);
% ad eps
K1 = squareform( pdist(samps1') );
eps_ad1 = median(K1(:));
eps_ad1 = Utils.checkEps(eps_ad1,K1,1);
K2 = squareform( pdist(samps2') );
eps_ad2 = median(K2(:));
eps_ad2 = Utils.checkEps(eps_ad2,K2,1);

% [n_labels_ad_reg, n_labels_rad_reg] = Utils.syncLabelImages(labels_ad_reg,labels_rad_reg,10);
% [n_labels_ad_sym, n_labels_rad_sym] = Utils.syncLabelImages(labels_ad_sym,labels_rad_sym,10);

create_eps_fig(rad_eps1_reg, rad_mad1_reg, rad_eps2_reg, rad_mad2_reg, eps_ad1, eps_ad2, img_1, img_2, labels_ad_reg, labels_rad_reg, title_in, "Regular Kernel")
create_eps_fig(rad_eps1_sym, rad_mad1_sym, rad_eps2_sym, rad_mad2_sym, eps_ad1, eps_ad2, img_1, img_2, labels_ad_sym, labels_rad_sym, title_in, "Symmetric Kernel")
end

function [] = create_eps_fig(rad_eps1, rad_mad1, rad_eps2, rad_mad2, eps_ad1, eps_ad2, img_1, img_2, labels_ad, labels_rad, title_in, type)
    L_rad = reshape(labels_rad, size(img_1));
    L_ad = reshape(labels_ad, size(img_1));
    [n_L_rad, n_L_ad] = Utils.syncLabelImages(L_rad,L_ad,10);
    figure;
    subplot(2,2,1);
    imshow(Utils.normTo01(img_1));
    title('first modality');
    subplot(2,2,2);
    imshow(Utils.normTo01(img_2));
    title('second modality');
    subplot(2,2,3);
    n_L_ad = label2rgb(n_L_ad);
    imagesc(n_L_ad);
    pbaspect([1 1 1]);
    title('naive AD');
    subplot(2,2,4);
    n_L_rad = label2rgb(n_L_rad);
    imagesc(n_L_rad);
    pbaspect([1 1 1]);
    title('refined AD');
    eps1_title = strcat("eps rad1 = ",num2str(rad_eps1),"+-",num2str(rad_mad1),"eps naive1 =", num2str(eps_ad1)," dist = ",num2str(abs(eps_ad1-rad_eps1)/rad_eps1)," [mad]");
    eps2_title = strcat("eps rad2 = ",num2str(rad_eps2),"+-",num2str(rad_mad2),"eps naive2 =", num2str(eps_ad2)," dist = ",num2str(abs(eps_ad2-rad_eps2)/rad_eps2)," [mad]");
    sgtitle ({type, strrep(title_in,"_"," "), eps1_title, eps2_title});
    currentFolder = pwd;
    output_directory = strcat(currentFolder, "\+clustering\results\figs\");
    savefig(strcat(output_directory,title_in,"_",strrep(type," ","_")));
end
