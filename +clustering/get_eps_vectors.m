function [ker_scale_1_reg, ker_scale_2_reg] = get_eps_vectors(patch_size,A_ct,A_lt)
rep_num = 25;
ker_scale_1_reg = zeros(rep_num,1);
ker_scale_2_reg = zeros(rep_num,1);
for rep_ind=1:rep_num
    cur_casi_patch = Utils.rand_patch(A_ct, patch_size);
    cur_lidar_patch = Utils.rand_patch(A_lt, patch_size);
    [cur_lidar_samp,cur_casi_samp] = clustering.images3d2ad_samps(cur_casi_patch, cur_lidar_patch);
    [ ker_scale_1_reg(rep_ind), ker_scale_2_reg(rep_ind)] = DM_AD.Refined_AD_get_eps( cur_casi_samp , cur_lidar_samp);
end
end