function [] = handleByPatch(OUTPUT_DIR, patch_sizes,A_ct,A_lt, casi_samp , lidar_samp, NUMBER_OF_EVAL_TO_USE, dim, pos, eps_ad1, eps_ad2, common_reg_ad, S_reg_ad)
%HANDLEBYPATCH Summary of this function goes here
%   Detailed explanation goes here
for patch_ind =1:length(patch_sizes)
    patch_size = [patch_sizes(patch_ind) patch_sizes(patch_ind)];
    % epsilon vecs
    [ker_scale_1_reg, ker_scale_2_reg] = clustering.get_eps_vectors(patch_size,A_ct,A_lt);
    % reg eps rad
    rad_eps1_reg = median(ker_scale_1_reg);
    rad_mad1_reg = mad(ker_scale_1_reg,1);
    rad_eps2_reg = median(ker_scale_2_reg);
    rad_mad2_reg = mad(ker_scale_2_reg,1);
    % get refined AD solutions
    [Pcom_reg_rad] = DM_AD.Basic_AD_knn( casi_samp , lidar_samp,-1,[rad_eps1_reg rad_eps2_reg],0,'regular');
    [common_reg_rad, S_reg_rad] = eigs(Pcom_reg_rad,dim);
    % title
    title1 = strcat("pos_",num2str(pos(1)),"_",num2str(pos(2)),"patch_size","_",num2str(patch_size(1)),"_",num2str(patch_size(2)));
    eps1_title = strcat("eps rad1 = ",num2str(rad_eps1_reg),"+-",num2str(rad_mad1_reg)," eps naive1 =", num2str(eps_ad1)," dist = ",num2str(abs(eps_ad1-rad_eps1_reg)/rad_mad1_reg)," [mad]");
    eps2_title = strcat("eps rad2 = ",num2str(rad_eps2_reg),"+-",num2str(rad_mad2_reg)," eps naive2 =", num2str(eps_ad2)," dist = ",num2str(abs(eps_ad2-rad_eps2_reg)/rad_mad2_reg)," [mad]");
    full_title = strcat(title1, eps1_title ,eps2_title);
    % plot results
    clustering.plot_result(OUTPUT_DIR, NUMBER_OF_EVAL_TO_USE, common_reg_rad, S_reg_rad, common_reg_ad, S_reg_ad, A_lt, A_ct, full_title)
end
end

