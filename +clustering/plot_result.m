function [] = plot_result(OUTPUT_DIR, num_of_eval, common_reg_rad, S_reg_rad, common_reg_ad, S_reg_ad, A_lt, A_ct, full_title)
%PLOT_RESULT Summary of this function goes here
%   Detailed explanation goes here
f_image_refined = clustering.getFImage(num_of_eval, common_reg_rad, S_reg_rad);
f_image_naive = clustering.getFImage(num_of_eval, common_reg_ad, S_reg_ad);
f_image_refined = reshape(f_image_refined,size(A_lt));
f_image_naive = reshape(f_image_naive,size(A_lt));
a = A_lt(:);
b = reshape(A_ct,size(A_ct,1)*size(A_ct,2),size(A_ct,3));
knn_image = clustering.rawKnn(num_of_eval,a,b);
figure;
subplot(2,3,1);
imshow(Utils.normTo01(A_lt));
title('first modality');
subplot(2,3,2);
imshow(Utils.normTo01(max(A_ct,[],3)-min(A_ct,[],3)));
title('second modality');
subplot(2,3,3);
imagesc(f_image_naive);
[c3,c4] = Utils.removeOutliers(f_image_naive);
caxis([c3 c4]);
title("f image naive");
subplot(2,3,4);
imagesc(f_image_refined);
[c1,c2] = Utils.removeOutliers(f_image_refined);
caxis([c1,c2]);
title("f image refined");
subplot(2,3,5);
imagesc( reshape(knn_image,size(A_lt)));
title("knn image");
sgtitle (full_title);
savefig(strcat(OUTPUT_DIR,full_title,".fig"));
end

