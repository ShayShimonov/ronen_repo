clear all;
close all;
%% read data
lidar_path = fullfile(pwd,'+clustering\GRSS2013\2013_DFTC\2013_IEEE_GRSS_DF_Contest_LiDAR.tif');
casi_path = fullfile(pwd,'+clustering\GRSS2013\2013_DFTC\2013_IEEE_GRSS_DF_Contest_CASI.tif');
[A_l,R_l] =  geotiffread(lidar_path);
[A_c,R_c] =  geotiffread(casi_path);
%% show res
% figure;
% mapshow(uint8(A_l),R_l);
% figure();
% for ii = 1:size(A_l,3)
%     imshow(A_c(:,:,ii),[])
%     hold on;
% end
%% trim to 100 * 100
pos = [50,560];
A_lt = Utils.normelizeByDim(double(A_l(pos(1):(pos(1)+99),pos(2):(pos(2)+99))),3);
A_ct = Utils.normelizeByDim(double(A_c(pos(1):(pos(1)+99),pos(2):(pos(2)+99),:)),3);
%% claulate in patches
patch_sizes = [10 25 35 50];
for ll =1:4
    patch_size = [patch_sizes(ll) patch_sizes(ll)];
    rep_num = 25;
    K =10;
    ker_scale_1_reg = zeros(rep_num,1);
    ker_scale_2_reg = zeros(rep_num,1);
    ker_scale_1_sym = zeros(rep_num,1);
    ker_scale_2_sym = zeros(rep_num,1);
    dim = patch_size(1)*patch_size(2);
    for kk=1:rep_num
        cur_casi_patch = Utils.rand_patch(A_ct, patch_size);
        cur_lidar_patch = Utils.rand_patch(A_lt, patch_size);
        [cur_lidar_samp,cur_casi_samp] = clustering.images3d2ad_samps(cur_casi_patch, cur_lidar_patch);
        [reg_ker_rad, ker_scale_1_reg(kk), ker_scale_2_reg(kk)] = DM_AD.Refined_AD_ker( cur_casi_samp , cur_lidar_samp,'regular');
        [cur_common_reg_rad, cur_S_reg_rad] = eigs(reg_ker_rad,dim);
        [sym_ker_rad, ker_scale_1_sym(kk), ker_scale_2_sym(kk)] = DM_AD.Refined_AD_ker( cur_casi_samp , cur_lidar_samp,'symmetric');
        [cur_common_sym_rad, cur_S_sym_rad] = eigs(sym_ker_rad, dim);
%         cur_reg_labels = clustering.getClustterLabels(K,cur_common_reg_rad);
%         cur_sym_labels = clustering.getClustterLabels(K,cur_common_sym_rad);
%         clustering.clustterData(cur_reg_labels, cur_lidar_patch(:,:,1), "reg refined ad");
%         clustering.clustterData(cur_sym_labels, cur_lidar_patch(:,:,1), "sym refined ad");
    end
    save(strcat("+clustering\results\pos_",num2str(pos(1)),"_",num2str(pos(1)),"normDim_wo_loc_patch_size",num2str(patch_size(1)),num2str(patch_size(2)),".mat"));
end
%% caluclate samples to AD
% [lidar_samp,casi_samp] = clustering.images3d2ad_samps(A_lt, A_ct);
% %% AD
% dim = 100;
% % [Pcom_reg_ad] = DM_AD.Basic_AD_spec_ker( casi_samp , lidar_samp,1,[0 0],0,'regular');
% % [common_reg_ad, S_reg_ad] = eigs(Pcom_reg_ad,dim);
% % [Pcom_sym_ad] = DM_AD.Basic_AD_spec_ker( casi_samp , lidar_samp,1,[0 0],0,'symmetric');
% % [common_sym_ad, S_sym_ad] = eigs(Pcom_sym_ad,dim);
% %% refined AD
% [Pcom_reg_rad] = DM_AD.Refined_AD_ker( casi_samp , lidar_samp,'regular');
% [common_reg_rad, S_reg_rad] = eigs(Pcom_reg_rad,dim);
% [Pcom_sym_rad] = DM_AD.Refined_AD_ker( casi_samp , lidar_samp,'symmetric');
% [common_sym_rad, S_sym_rad] = eigs(Pcom_sym_rad,dim);
% %% labels of AD
% K = 10;
% % reg_ad_l = clustering.getClustterLabels(K,common_reg_ad);
% % sym_ad_l = clustering.getClustterLabels(K,common_sym_ad);
% %% labels of refined AD
% reg_rad_l = clustering.getClustterLabels(K,common_reg_rad);
% sym_rad_l = clustering.getClustterLabels(K,common_sym_rad);
% %% show res AD
% % clustering.clustterData(reg_ad_l, A_lt, "reg ad");
% % clustering.clustterData(sym_ad_l, A_lt, "sym ad");
% %% show res refined AD
% clustering.clustterData(reg_rad_l, A_lt, "reg refined ad no location");
% clustering.clustterData(sym_rad_l, A_lt, "sym refined ad no location");
%% save results
% save(strcat("+clustering\results\norm01_wo_loc_patch_size",num2str(patch_size(1)),num2str(patch_size(2)),".mat"));
