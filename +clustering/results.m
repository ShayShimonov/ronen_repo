close all;
clear all;
%% 
directory = "C:\Users\shayshi\Documents\ronen_repo\+clustering\results\patch_size";
output_directory = "C:\Users\shayshi\Documents\ronen_repo\+clustering\results\res";
files = dir(strcat(directory, '\*.mat'));
L = length (files);
for i=1:L
    load(strcat(directory,'\',files(i).name));
    if startsWith(files(i).name, "pos") == 0
        continue
    end
    [~,fname,~] = fileparts(files(i).name);
   % caluclate samples to AD
   [lidar_samp,casi_samp] = clustering.images3d2ad_samps(A_lt, A_ct);
    dim = 500;
    reg_eps1 = median(ker_scale_1_reg);
    std_reg_eps1 = mad(reg_eps1,1);
    reg_eps2 = median(ker_scale_2_reg);
    std_reg_eps2 = mad(reg_eps2,1);
    sym_eps_1 = median(ker_scale_1_sym);
    std_sym_eps1 = mad(sym_eps_1,1);
    sym_eps_2 = median(ker_scale_2_sym);
    std_sym_eps2 = mad(sym_eps_2,1);
    [Pcom_reg_rad] = DM_AD.Basic_AD_spec_ker( casi_samp , lidar_samp,-1,[reg_eps1 reg_eps2],0,'regular');
    [common_reg_rad, S_reg_rad] = eigs(Pcom_reg_rad,dim);
    [Pcom_sym_rad] = DM_AD.Basic_AD_spec_ker( casi_samp , lidar_samp,-1,[sym_eps_1 sym_eps_2],0,'symmetric');
    [common_sym_rad, S_sym_rad] = eigs(Pcom_sym_rad,dim);
    [Pcom_reg_ad] = DM_AD.Basic_AD_spec_ker( casi_samp , lidar_samp,1,[0 0],0,'regular');
    [common_reg_ad, S_reg_ad] = eigs(Pcom_reg_ad,dim);
    [Pcom_sym_ad] = DM_AD.Basic_AD_spec_ker( casi_samp , lidar_samp,1,[0 0],0,'symmetric');
    [common_sym_ad, S_sym_ad] = eigs(Pcom_sym_ad,dim);
    % labels of AD
    K = 10;
    reg_ad_l = clustering.getClustterLabels(K,common_reg_ad);
    sym_ad_l = clustering.getClustterLabels(K,common_sym_ad);
    % labels of refined AD
    reg_rad_l = clustering.getClustterLabels(K,common_reg_rad);
    sym_rad_l = clustering.getClustterLabels(K,common_sym_rad);
    % show res AD
    title_ad =  strcat("NaiveAD:" , fname);
    clustering.clustterData2(reg_ad_l, A_lt(:,:,1), strcat(title_ad, " REGULAR"),output_directory, strcat(fname,"_ad_reg"));
    clustering.clustterData2(sym_ad_l, A_lt(:,:,1), strcat(title_ad, " SYMMETRIC"),output_directory, strcat(fname,"_ad_sym"));
    % show res refined AD
    title_reg = strcat("RefinedAD:" , fname, " REGULAR. eps1:", num2str(reg_eps1)," eps2: ", num2str(reg_eps2), "std1: ", num2str(std_reg_eps1), "std1: " , num2str(std_reg_eps2)); 
    title_ad = strcat("RefinedAD:" , fname, " SYMMETRIC. eps1:", num2str(sym_eps_1)," eps2: ", num2str(sym_eps_2), "std1: ", num2str(std_sym_eps1), "std1: " , num2str(std_sym_eps2)); 
    clustering.clustterData2(reg_rad_l, A_lt(:,:,1), title_reg,output_directory, strcat(fname,"_rad_reg"));
    clustering.clustterData2(sym_rad_l, A_lt(:,:,1), title_ad,output_directory, strcat(fname,"_rad_sym"));
    save(strcat(output_directory,"\",fname, "_res.mat"));
end