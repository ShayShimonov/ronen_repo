function [ delta  ] = deltaGen( diffused_points, dim )
%{
-Function name: deltaGen
-Description:
* calculates delta by taking the biggest ciclic distance between
  two diffused points on the sensor specific dimention 
-Inputs:
* diffused_points - all diffused points group
* dim - sensor specific dimention 
-Outputs:
* delta - calculated delta value
-Reurn values:
* None
-Notes:
* threshold for both iterations is the same
%}
so =  sort(diffused_points(dim,:)') ;
d = [diff(so);1+so(1)-so(end)];
delta = max(d); %worst case
end

