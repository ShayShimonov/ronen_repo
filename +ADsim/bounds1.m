function [ ] = bounds1( numberOfSim, bounds )
% prints graphs of bounds
% bounds - row vector which specifies disturpution upper bound assuming lower bound is zero
% numberOfSim - index of disired simulation
import Utils.* DM_AD.* ADsim.*
variances= 1/12.*(bounds.^2);
load(['+ADsim\sim',num2str(numberOfSim),'.mat']);
n1th= 3000*(median(eps1Sim))^(2);
n2th= ((3000)^2)*(median(eps1Sim)*median(eps2Sim))^(2);
Ddelta1 = 1./delta1Sim;
denom1 = Ddelta1.*log(Ddelta1);
Bound1nth = exp(-n1th./denom1);
Bound1nreal = exp(-n1Sim./denom1);
Ddelta2 = 1./delta2Sim;
denom2 = Ddelta2.*log(Ddelta2);
Bound2nth = exp(-n2th./denom2);
Bound2nreal = exp(-n2Sim./denom2);
%Bound2try = exp(-n2Sim./(denom2.*variances(2)));
[del1,id1] = sort(delta1Sim);
figure()
scatter(1:100,del1./bounds(3));
hold on;
scatter(1:100,Bound1nth(id1));
scatter(1:100,Bound1nreal(id1));
title('\delta_z simulation resault');
hold off;
legend('prob','Bound nth','Bound n real');
[del2,id2] = sort(delta2Sim);
figure()
scatter(1:100,del2./bounds(2));
hold on;
scatter(1:100,Bound2nth(id2));
scatter(1:100,Bound2nreal(id2));
%scatter(1:100,Bound2try(id2));
title('\delta_y simulation resault');
hold off;
legend('prob','Bound nth','Bound n real');
% legend('prob','Bound nth','Bound n real','try');
end

