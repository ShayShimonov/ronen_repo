function [delta1Sim,eps1Sim,delta2Sim,eps2Sim,n1Sim,n2Sim ] = sims_mod( nS,n,display,bounds,LoadEpsSim,par )
%{
-Function name: sims
-Description: 
* preforms simulations of AD algorithem on uniform disterbution
  with some changes: 1.on second iteration we use thresh^2 instead of thresh.
                     2. we turn the final disterbution to a binary one
-Inputs:
* n - number of points in distrebution
* nS - number of simulation iterations
* display - if true display plots
* bounds - row vector which specifies disturpution upper bound assuming lower bound is zero
* LoadEpsSim - if true disterbution is taken from epsilon simulation number LoadEpsSim
* par - if not null use par as a struct which contains simulation variables
        (eps , xz, xy, ReverseOrder)
-Outputs:
* delta1Sim - values from simulations
* eps1Sim - values from simulations
* delta2Sim - values from simulations
* eps2Sim - values from simulations
* n1Sim - values from simulations
* n2Sim - values from simulations
-Reurn values:
* None
-Notes:
* threshold for both iterations is the same
%}
import Utils.* DM_AD.* ADsim.*
delta1Sim = zeros(nS,1);
eps1Sim = zeros(nS,1);
delta2Sim = zeros(nS,1);
eps2Sim = zeros(nS,1);
n1Sim =  zeros(nS,1);
n2Sim = zeros(nS,1);
threshold = 0.01;
for kk=1:nS
    % 3D uniform distribution
    if(LoadEpsSim)
        load(['+OptEpsSim\epsSim',num2str(LoadEpsSim)]);
        u3 = [xy;xz(2,:)];
        n = size(xy,2);
    elseif nargin == 6
        xy = par.xy;
        xz = par.xz;
        u3 = [xy;xz(2,:)];
        n = size(xy,2);
    else
        u3 = [bounds(1)*rand(1,n);bounds(2)*rand(1,n);bounds(3)*rand(1,n)];
        xy = u3(1:2,:);
        xz = u3([1 3],:);
    end
    % compute karnels
    if nargin == 5
        [K1,eps1] = Basic_DM_mod( xy,0.1,bounds(1:2));
        [K2,eps2] = Basic_DM_mod( xz,0.1,bounds([1 3]));
        par.ReverseOrder =0;
    else
        [K1,eps1] = Basic_DM_mod( xy,-1,bounds(1:2),par.epsilons(1));
        [K2,eps2] = Basic_DM_mod( xz,-1,bounds([1 3]),par.epsilons(2));
    end
    S = [1; zeros(n-1,1)];
    ind =1;
    % epsAv = 0.5*(eps1+eps2);
    epsAv = min(eps1,eps2);
    % plot process
    delta=1;
    delta2=1;
    for ii =1:(2+(display~=0)) % for sim i=1:2 ow i=1:3
        %display
        if(display)
            figure(1)
            p11 = scatter(u3(1,:),u3(2,:),'b');
            hold on;
            p12 = scatter(u3(1,ind),u3(2,ind),'*r');
            p12.LineWidth=3;
            hold off;
            title(['X-Y plane- \delta_2:', num2str(delta2)],'FontSize',22);
            axis equal;
            figure(2)
            p21 = scatter(u3(1,:),u3(3,:),'b');
            hold on;
            p22 = scatter(u3(1,ind),u3(3,ind),'*r');
            p22.LineWidth=3;
            title(['X-Z plane- \delta_1:', num2str(delta)],'FontSize',22);
            hold off;
            axis equal;
            k = waitforbuttonpress;
        end
        %process
%         if mod(ii,2)==double(nargin == 5 || (nargin ==6 && ~par.ReverseOrder))% regular order if not reverse order instracted
        if mod(ii,2)==1
            if(~par.ReverseOrder)
            S = K1*S;
            else
               S = K2*S;
            end
            %      ind = find(S>0.01*epsAv);
            ind = find(S>threshold*epsAv); % for delta sim
            delta = deltaGen(u3(:,ind),3);
            n1 = length(ind);
            S(~ind) = 0;
        else
            if(par.ReverseOrder)
            S = K1*S;
            else
               S = K2*S;
            end
%             ind = find(S>0.01*epsAv);
            ind = find(S>(threshold^2)*epsAv); % for delta sim
            delta2 = deltaGen(u3(:,ind),2);
            n2 =  length(ind);
            S(~ind) = 0;
        end
        
    end
    S(S~=0) =1;
    %loop of simulation
    delta1Sim(kk) = delta;
    eps1Sim(kk) = eps1;
    delta2Sim(kk) = delta2;
    eps2Sim(kk) = eps2;
    n1Sim(kk) =  n1;
    n2Sim(kk) = n2;
end

end

