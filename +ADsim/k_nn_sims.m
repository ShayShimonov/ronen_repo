function [delta1Sim,eps1Sim,delta2Sim,eps2Sim,n1Sim,n2Sim ] = k_nn_sims(par)
%{
-Function name: sims
-Description: preforms simulations of AD algorithem on uniform disterbution
-Inputs:
* n - number of points in distrebution
* nS - number of simulation iterations
* display - if true display plots
* bounds - row vector which specifies disturpution upper bound assuming lower bound is zero
* LoadEpsSim - if true disterbution is taken from epsilon simulation number LoadEpsSim
* par - if not null use par as a struct which contains simulation variables
        (eps , xz, xy, ReverseOrder)
-Outputs:
* delta1Sim - values from simulations
* eps1Sim - values from simulations
* delta2Sim - values from simulations
* eps2Sim - values from simulations
* n1Sim - values from simulations
* n2Sim - values from simulations
-Reurn values:
* None
-Notes:
* threshold for both iterations is the same
%}

%% imports
import Utils.* DM_AD.* ADsim.*
%% constants
MEDIAN_DIVIDER = 0.1;
TRESHOLD = 0.01;

%% body
% 3D uniform distribution
xy = par.xy;
xz = par.xz;
u3 = [xy;xz(2,:)];
n = size(xy,2);
% compute karnels
[K1,eps1] = Basic_DM_mod( xy,-1,par.bounds(1:2),par.epsilons(1));
[K2,eps2] = Basic_DM_mod( xz,-1,par.bounds([1 3]),par.epsilons(2));
current_disterbution = [1; zeros(n-1,1)];
% plot process
delta=1;
delta2=1;
if(par.ReverseOrder == 0)
    eps_first = eps1;
    K_first =  K1;
    eps_last = eps2;
    K_last = K2;   
else
    eps_first = eps2;
    K_first =  K2;
    eps_last = eps1;
    K_last = K1;
end
for ii =1:2
    %process
    if mod(ii,2)==1% regular order if not reverse order instracted
        current_disterbution = K_first*current_disterbution;
        [~,ind] = sort(current_disterbution, 'descend');
        n1 = floor(n* eps_first^2);
        ind = ind(1:n1);
        temp =  zeros(size(current_disterbution));
        temp(ind) = 1;
        current_disterbution = (1/n1) * temp;
        delta = deltaGen(u3(:,ind),3);
    else
        n2 =  floor(n^2* eps_first^2*eps_last^2);
        if n2>n %saturate
            n2 = n;
        end
        current_disterbution = K_last*current_disterbution;
        [~,ind] = sort(current_disterbution, 'descend');
        ind = ind(1:n2);
        temp =  zeros(size(current_disterbution));
        temp(ind) = 1;
        current_disterbution = (1/n2) * temp;
        delta2 = deltaGen(u3(:,ind),2);   
    end
    assert(sum(current_disterbution)-1<(1e-3));
end
    
%loop of simulation
delta1Sim = delta;
eps1Sim = eps1;
delta2Sim = delta2;
eps2Sim = eps2;
n1Sim =  n1;
n2Sim = n2;

end

