function [delta1Sim,eps1Sim,delta2Sim,eps2Sim,n1Sim,n2Sim ] = sims( nS,n,display,bounds,LoadEpsSim,par )
%{
-Function name: sims
-Description: preforms simulations of AD algorithem on uniform disterbution
-Inputs:
* n - number of points in distrebution
* nS - number of simulation iterations
* display - if true display plots
* bounds - row vector which specifies disturpution upper bound assuming lower bound is zero
* LoadEpsSim - if true disterbution is taken from epsilon simulation number LoadEpsSim
* par - if not null use par as a struct which contains simulation variables
        (eps , xz, xy, ReverseOrder)
-Outputs:
* delta1Sim - values from simulations
* eps1Sim - values from simulations
* delta2Sim - values from simulations
* eps2Sim - values from simulations
* n1Sim - values from simulations
* n2Sim - values from simulations
-Reurn values:
* None
-Notes:
* threshold for both iterations is the same
%}

%% imports
import Utils.* DM_AD.* ADsim.*
%% constants
MEDIAN_DIVIDER = 0.1;
TRESHOLD = 0.01;
EPS_SIM_PATH = 'simulationFiles\optEps\old\epsSim';

%% body
delta1Sim = zeros(nS,1);
eps1Sim = zeros(nS,1);
delta2Sim = zeros(nS,1);
eps2Sim = zeros(nS,1);
n1Sim =  zeros(nS,1);
n2Sim = zeros(nS,1);
for kk=1:nS
    % 3D uniform distribution
    if(LoadEpsSim)
        load([EPS_SIM_PATH,num2str(LoadEpsSim)]);
        u3 = [xy;xz(2,:)];
        n = size(xy,2);
    elseif nargin == 6
        xy = par.xy;
        xz = par.xz;
        u3 = [xy;xz(2,:)];
        n = size(xy,2);
    else
        u3 = [bounds(1)*rand(1,n);bounds(2)*rand(1,n);bounds(3)*rand(1,n)];
        xy = u3(1:2,:);
        xz = u3([1 3],:);
    end
    % compute karnels
    if nargin == 5
        [K1,eps1] = Basic_DM_mod( xy,MEDIAN_DIVIDER,bounds(1:2));
        [K2,eps2] = Basic_DM_mod( xz,MEDIAN_DIVIDER,bounds([1 3]));
    else
        [K1,eps1] = Basic_DM_mod( xy,-1,bounds(1:2),par.epsilons(1));
        [K2,eps2] = Basic_DM_mod( xz,-1,bounds([1 3]),par.epsilons(2));
    end
    current_disterbution = [1; zeros(n-1,1)];
    ind =1;
    % plot process
    delta=1;
    delta2=1;
    eps_first = eps1;
    K_first =  K1;
    eps_last = eps2;
    K_last = K2;   
    if (nargin==6 && par.ReverseOrder)
        eps_first = eps2;
        K_first =  K2;
        eps_last = eps1;
        K_last = K1;
    end
    for ii =1:(2+(display~=0)) % for sim i=1:2 ow i=1:3
        %display
        if(display)
            figure();
            p11 = scatter(u3(1,:),u3(2,:),'MarkerEdgeColor',([0,0,0]+0.1),'MarkerEdgeAlpha',0.3);
            hold on;
            p12 = scatter(u3(1,ind),u3(2,ind),'*r');
            p12.LineWidth=3;
            hold off;
%             title(['X-Y plane- \delta_2:', num2str(delta2)],'FontSize',22);
            title('S1 plane X-Y','FontSize',22);
            xlabel('common variable - X','FontSize',18);
            ylabel('sensor specific variable - Y','FontSize',18);
%             axis equal;
            
            figure();
            p21 = scatter(u3(1,:),u3(3,:),'MarkerEdgeColor',([0,0,0]+0.1),'MarkerEdgeAlpha',0.3);
            hold on;
            p22 = scatter(u3(1,ind),u3(3,ind),'*r');
            p22.LineWidth=3;
%             title(['X-Z plane- \delta_1:', num2str(delta)],'FontSize',22);
            title('S2 plane X-Z','FontSize',22);
            xlabel('common variable - X','FontSize',18);
            ylabel('sensor specific variable - Z','FontSize',18);
            hold off;
%             axis equal;
            %             k = waitforbuttonpress;
        end
        %process       
        if mod(ii,2)== 1
            current_disterbution = K_first*current_disterbution;
            ind = find(current_disterbution> TRESHOLD*eps_first);
            delta = deltaGen(u3(:,ind),3);
            n1 = length(ind);
        else
            current_disterbution = K_last*current_disterbution;
            ind = find(current_disterbution> TRESHOLD^2*eps_last);
            delta2 = deltaGen(u3(:,ind),2);
            n2 =  length(ind);
        end
        assert(sum(current_disterbution)-1<(1e-3));
    end
    
    %loop of simulation
    delta1Sim(kk) = delta;
    eps1Sim(kk) = eps1;
    delta2Sim(kk) = delta2;
    eps2Sim(kk) = eps2;
    n1Sim(kk) =  n1;
    n2Sim(kk) = n2;
end

end

