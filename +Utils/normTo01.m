function [norm_sig] = normTo01(sig)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
max_sig = max(sig(:));
min_sig = min(sig(:));
norm_sig = (sig-min_sig)./(max_sig-min_sig);
end

