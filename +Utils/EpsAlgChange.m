function [eps1,eps2] = EpsAlgChange(n,c,dims)
%function calculates AD optimal Epsilon's Kernel value.
% n - number of points in disterution
% c - 1 by 2 array of constants handles of each procees
% dims - process dimentions where as dx is the reconstuction requested
% dimention
import Utils.*
% aSigm =20/n; bSigm = 3*n;
% ineq1 =@(delta) delta +((1/(c(1)*n))*delta^(-dims(3))*(1/delta-1))^(dims(1)/(dims(3)*(dims(1)+dims(2))));
% delta1=fminbnd(ineq1,0,1);
% %original
% %  ineq2 = @(delta) delta1 +((c(1)/c(2))*(1/n)*(delta1^(dims(3)+1))/(delta^(dims(2)+1))*(1-delta)/(1-delta1))^(dims(1)/(dims(2)*(dims(1)+dims(3))));
% ineq2 = @(delta) delta +(isigmf(((1/(c(2)*n))*delta^(-dims(2))*(1/delta-1)),aSigm,bSigm)/((1/(c(1)))*delta1^(-dims(3))*(1/delta1-1))...
%     +((1/(c(1)*n))*delta1^(-dims(3))*(1/delta1-1))^(1/(dims(1)+dims(2))))^(dims(1)/(dims(2)));
% delta2=fminbnd(ineq2,0,1);
% eps1 = ((1/(c(1)*n))*delta1^(-dims(3))*(1/delta1-1))^(1/(dims(1)+dims(2)));
% beta = ((1/(c(2)*n))*delta2^(-dims(2))*(1/delta2-1));
% eps2 = (isigmf(beta,aSigm,bSigm)/((1/(c(1)))*delta1^(-dims(3))*(1/delta1-1)))^(1/(dims(1)+dims(3)));
aSigm =20; bSigm = 0.1;
ineq1 =@(delta) delta +((1/(c(1)*n))*delta^(-dims(3))*(1/delta-1))^(dims(1)/(dims(3)*(dims(1)+dims(2))));
delta1=fminbnd(ineq1,0,1);
%original
%  ineq2 = @(delta) delta1 +((c(1)/c(2))*(1/n)*(delta1^(dims(3)+1))/(delta^(dims(2)+1))*(1-delta)/(1-delta1))^(dims(1)/(dims(2)*(dims(1)+dims(3))));
ineq2 = @(delta) delta +(isigmf(((1/(c(2)*n))*delta^(-dims(2))*(1/delta-1)),aSigm,bSigm)/((1/(c(1)))*delta1^(-dims(3))*(1/delta1-1))...
    +((1/(c(1)*n))*delta1^(-dims(3))*(1/delta1-1))^(1/(dims(1)+dims(2))))^(dims(1)/(dims(2)));
delta2=fminbnd(ineq2,0,1);
eps1 = ((1/(c(1)*n))*delta1^(-dims(3))*(1/delta1-1))^(1/(dims(1)+dims(2)));
beta = ((1/(c(2)*n))*delta2^(-dims(2))*(1/delta2-1));
eps2 = (isigmf(beta,aSigm,bSigm)/((1/(c(1)))*delta1^(-dims(3))*(1/delta1-1)))^(1/(dims(1)+dims(3)));

end
