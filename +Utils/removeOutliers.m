function [min_a, max_a] = removeOutliers(in_image)
%REMOVEOUTLIERS Summary of this function goes here
%   Detailed explanation goes here
[r_image, ~] = rmoutliers(in_image(:));
max_a = max(r_image(:));
min_a = min(r_image(:));
% out_image = in_image;
% out_image(in_image>max_a) = max_a;
% out_image(in_image<min_a) = min_a;
end

