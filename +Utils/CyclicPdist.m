function [ K ] = CyclicPdist( samps,bounds, dims)
% bounds is a row vector which specifies disturpution upper bound
% assuming lower bound is zero
[dim,N] = size(samps);
bound2use =[];
for kk= 1: length(bounds)
    bound2use = [bound2use, repmat(bounds(kk),1,dims(kk))];
end
Kssum = zeros(N);
for kk=1:dim
    Ktemp =  squareform( pdist(samps(kk,:)') );
    KMtemp = min(Ktemp,bound2use(kk)-Ktemp);
    Kssum = Kssum +KMtemp.^2;
end
K = sqrt(Kssum);
end

