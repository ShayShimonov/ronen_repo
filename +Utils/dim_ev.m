function [dim] = dim_ev(samps)
% the function gets an array of samples (multi dim signal) and returns the
% effective evaluated dimention
%   Detailed explanation goes here
[dimB,~] = size(samps);
[~,Sd]=DM_AD.Basic_DM_spec( samps,1,0.1,ones(dimB,1),-1);
[~,dim] = min(abs(Sd-0.5*(Sd(1))));
% [dim,~] = Utils.knee_pt(Sd);
end

