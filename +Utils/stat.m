function [min_s,max_s,mean_s,std_s] = stat(samps)
%STAT Summary of this function goes here
%   Detailed explanation goes here
min_s = min(samps);
max_s = max(samps);
mean_s = mean(samps);
std_s = std(samps);
end

