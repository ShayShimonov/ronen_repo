function [Knn] = knn_kernel(samps,kernel_scale)
%this function creates a diffussion kernel uning only k-nn as desired.
%input:
%samps - data sample set
%eps - kernel scale
import Utils.*
%NOTE: sum of coloums =1 
[~,N] = size(samps);
samps = samps';
K_init = squareform( pdist(samps) );
k = min(max(floor(N* kernel_scale^2),1),N);
%note the +1 is because the distance to self is counted
knn_mask = Utils.pdist2knn_mask(K_init,k+1);
W = exp(-K_init.^2/kernel_scale^2);
W = W.*knn_mask;
w_inv1 = diag((1./sum(W)));
Knn = W*w_inv1;
end

