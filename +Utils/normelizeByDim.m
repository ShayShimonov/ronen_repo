function [norm_sig] = normelizeByDim(sig,dim)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
dims = 1:length(size(sig));
if dim <= length(dims)
    dims(dim) = [];
end
max_sig = max(sig,[],dims);
min_sig = min(sig,[],dims);
norm_sig = (sig-min_sig)./(max_sig-min_sig);
end

