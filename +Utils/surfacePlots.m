function [ ] = surfacePlots(x,y,surf1 , surf2, surf3, surf4, surf5)
%{
-Function name: surfacePlots
-Description:
* plots 3D plot for multiple surfaces sharing same axes
-Inputs:
* x- first axes
* y- second axes
* surf 1-4: input surfaces
-Outputs:
* plot
-Reurn values:
* None
-Notes:
* can handle from 2-4 surfaces
%}
assert(nargin >= 4, 'function takes two axes and two surfaces at least')
figure();
a1 =surf(x,y,surf1);
hold on;
a2 = surf(x,y,surf2);
a1.FaceColor = 'b';
a2.FaceColor = 'm';
if (nargin >4)
    a3 = surf(x,y,surf3);
    a3.FaceColor = 'c';
end
if (nargin >5)
    a4 = surf(x,y,surf4);
    a4.FaceColor = 'y';
end
if (nargin >6)
    a5 = surf(x,y,surf5);
    a5.FaceColor = 'g';
end
end
