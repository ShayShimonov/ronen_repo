function [out_image1,out_image2] = syncLabelImages(label_image1,label_image2, labels_number)
%SYNCLABELIMAGES Summary of this function goes here
%   Detailed explanation goes here
used_labels = [];
out_image2 = zeros(size(label_image2));
for kk = 1: labels_number
   label_mask = (label_image1 == kk);
   label_cross = label_image2(label_mask);
   [n,bin] = hist(label_cross(:),unique(label_cross(:)));
   [~,idx] = sort(-n);
   frq_occ = bin(idx);
   for ll=1:length(frq_occ)
       if ~ismember(frq_occ(ll), used_labels)
           label_mask_2 = (label_image2 == frq_occ(ll));
           used_labels = [used_labels, frq_occ(ll)];
           out_image2(label_mask_2) = kk;
           break;
       end
   end
end
if (length(used_labels) ~= labels_number)
    labels = 1:labels_number;
    ind_not_used = ~ismember(labels, used_labels);
    not_used = labels(ind_not_used);
    original_not_used = labels(~ismember(labels, out_image2));
    for nn = 1:length(not_used)
        out_image2(label_image2==not_used(nn)) = original_not_used(nn);
    end
end
out_image1 = label_image1;
assert(~ismember(0,out_image2))
end
        


