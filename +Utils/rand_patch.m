function [out_patch] = rand_patch(input_img, patch_size)
%RAND_PATCH Summary of this function goes here
%   Detailed explanation goes here
m = size(input_img,1);
n = size(input_img,2);
assert(patch_size(1)<m);
assert(patch_size(2)<n);
m_p = randsample(m-patch_size(1),1);
n_p = randsample(n-patch_size(2),1);
out_patch = input_img((m_p:(m_p+patch_size(1))), (n_p:(n_p + patch_size(2))),:);
end

