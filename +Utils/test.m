%{Description: in This Script we will compare between different n1,n2 guesses in a practical way using real simulations}% imports
import Utils.* deltaBoundSim.*
%% constants
FILEPATH = 'D:\Users\Shay\Documents\ronen_repo\simulationFiles\deltaBound\old\';
SIM_NUMBER= 1
%% Load files
load([(strcat(FILEPATH, 'deltaSim')),num2str(SIM_NUMBER)]);
NUMBER_OF_POINTS=size(xy,2);
%% calc
[epss2,epss1] = meshgrid(epss);
n1_first_guess = NUMBER_OF_POINTS.*epss1.^((dims(1)+dims(2)));
% n1_first_guess = NUMBER_OF_POINTS.*(sqrt(-2*log(0.01*epss1*sqrt(2*pi)))).^((dims(1)+dims(2)));
n2_guess_2 = NUMBER_OF_POINTS*sqrt(epss1).^((dims(1)+dims(2)));
% surfacePlots(epss1,epss2,n1_first_guess , n1Sim)
figure();
plot(epss,(NUMBER_OF_POINTS.*epss.^((dims(1)+dims(2)))));
hold on;
n1Sim_mean = mean(n1Sim,2);
plot(epss,n1Sim_mean);
plot(epss, n2_guess_2);
title('n1')
%% r
r_eps1 = (n1Sim_mean/NUMBER_OF_POINTS).^(1/(dims(1)+dims(2)));
figure();
plot(epss,r_eps1);
title("relation between diffussion rate and radius of diffussion")
%% ff
epss1_half = epss1(epss<=0.5,epss<=0.5);
epss2_half = epss2(epss<=0.5,epss<=0.5);
n1_second_guess = 4 *NUMBER_OF_POINTS.*epss1_half.^((dims(1)+dims(2)));
% surfacePlots(epss1(epss<=0.5,epss<=0.5),epss2(epss<=0.5,epss<=0.5),n1_second_guess , n1Sim(epss<=0.5,epss<=0.5))
figure()
plot(epss(epss<=0.5),(4 *NUMBER_OF_POINTS.*(epss(epss<=0.5)).^((dims(1)+dims(2)))));
hold on;
title('n1 half')
%% n2
plot(epss(epss<=0.5),n1Sim_mean(epss<=0.5));
n2_guess_1 = NUMBER_OF_POINTS.*(epss1+epss2).^((dims(1)+dims(3)));
n2_guess_1_half = 4*NUMBER_OF_POINTS.*(epss1_half+epss2_half).^((dims(1)+dims(3)));
n2_guess_2 = (NUMBER_OF_POINTS^2).*(epss1).^((dims(1)+dims(2))).*(epss2).^((dims(1)+dims(3)));
n2_guess_2_half = 16*(NUMBER_OF_POINTS^2).*(epss1_half).^((dims(1)+dims(2))).*(epss2_half).^((dims(1)+dims(3)));
n2_guess_3 = (NUMBER_OF_POINTS).*(epss1+epss2).^(dims(1)).*((1-deltaYSim)/2).^(dims(3));
n2_guess_3_half =  4*(NUMBER_OF_POINTS).*(epss1_half+epss2_half).^(dims(1)).*((1-deltaYSim(epss<=0.5,epss<=0.5))/2).^(dims(3));
surfacePlots(epss1,epss2,n2Sim,n2_guess_1,n2_guess_2);
title('n2');
legend('real','g1','g3');
surfacePlots(epss1_half,epss2_half,n2Sim(epss<=0.5,epss<=0.5),n2_guess_1_half,n2_guess_3_half);
title('n2 half');
legend('real','g1','g3');

