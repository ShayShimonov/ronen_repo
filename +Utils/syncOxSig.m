function [ fsig1,fsig2 ] = syncOxSig( sig1,locs1,sig2,locs2 )
% the function syncs starting time of signals to first full cycle bound and
% signals lengths
%  sig1,2 -oximeter signals
% locs 1,2 - location of cycle bounds
tsig1 = sig1(locs1(1):end);
tsig2 = sig2(locs2(1):end);
[ fsig1,fsig2 ] = Utils.CutbyShort(tsig1,tsig2);
end

