function [knn_mask] = pdist2knn_mask(pdist_mat,k)
%PDIST2KNN Summary of this function goes here
%   Detailed explanation goes here
knn_mask = zeros(size(pdist_mat));
for col=1:size(pdist_mat,2)
    curr_col= pdist_mat(:,col);
    [~,knn_col_ind] = mink(curr_col,k);
    knn_mask(knn_col_ind,col) = 1;
end
knn_mask = knn_mask & knn_mask';
end

