function [xy,xz] = UniformDist( n ,bounds , dims )
%UNTITLED Summary of this function goes here
%  bounds - row vector which specifies disturpution upper bound assuming lower bound is zero
% dims - row vector which specifies each hidden vector dimention
if nargin == 1
    bounds = [1 1 1];
    dims = [1 1 1];
end
if nargin ==2
    dims = [1 1 1];
end
u3 = [bounds(1)*rand(dims(1),n);bounds(2)*rand(dims(2),n);bounds(3)*rand(dims(3),n)];
xy = u3(1:(dims(1)+dims(2)),:);
xz = u3([(1:dims(1)), (dims(1)+dims(2)+1:dims(1)+dims(2)+dims(3))],:);
end

