function [ USsur, SatInd,area] = CutSaturationArea(sur)
%this function recieves a 2D surface which may be saturated
% the function exemanes the function gradient magnitude and crops saturated
% areas.
% sur - surface to handle
% USsur - Un saturated surface (saturated areas contain zeros);
% SatInd - N x 2 which cotains the indices of croped saturated areas
% area - smallest relevent area
gradx = imfilter(sur,fspecial('sobel'),'symmetric');
grady = imfilter(sur,(fspecial('sobel'))','symmetric');
grad = sqrt(gradx.^2+grady.^2);
% indS = find(grad==0);
  indS = double(grad<0.1);
%  surf(sur);
% figure
% imagesc(indS); % all flat gradient area 
CC = bwconncomp(indS); % connected compoments in indS
numPixels = cellfun(@numel,CC.PixelIdxList);
[~,idx] = max(numPixels);
SatInd = CC.PixelIdxList{idx};
numPixels(idx) = 0;
[~,idx2] = max(numPixels);
SatInd = [SatInd; CC.PixelIdxList{idx2}];
% here the function assumes that the saturated areas are the largest two connected compoments in indS
% this assumption is not always correct, it is better to check if the
% coneected compuments boundries match the image bounderier but my sim acts
% by this assumption and it is more complicated to do it the oter way
map = ones(size(sur));
map(SatInd)=0;
[row,col] = find(map);
area = [min(row),max(row),min(col),max(col)];
USsur = sur;
USsur(SatInd) =nan;
USsur = USsur(area(1):area(2),area(3):area(4));
% figure
% surf(USsur);
end
