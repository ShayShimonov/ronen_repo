function [] = idenitySim()
%IDENITYSIM Summary of this function goes here
%   Detailed explanation goes here
recDim = 1;
[samps1,~] = UniformDist( 1000 ,[1 1 1], [1 1 1]);
x = samps1(1,:);
samps2 = x.^3+4*x+7;
% x_rec = Basic_DM_knn(samps1,1);
import Utils.* DM_AD.*
allSig = [samps1(:),samps1(:)];
vStd = std(allSig);
n = size(samps1,2);
if(vStd(1)>=vStd(2))
     c = vStd;
else
     c = flip(vStd);
end   
 dims = [1,1,0];
[eps1,eps2] = EpsAlg(n,c,dims);
common_rAD = Basic_AD_mod( samps1 , samps2, recDim,-1,[eps1,eps2],0 );
common = Basic_AD(samps1, samps2, 1);
 figure;
 scatter(x,common_rAD,'b');
 title('reconstraction vs real');
 xlabel('real');
 ylabel('rec');
 hold on;
 scatter(x,common,'r');
 legend('refined AD','naive AD');
end

